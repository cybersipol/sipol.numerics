﻿using System;
using NUnit.Framework;

namespace Tests.Unit
{
    [TestFixture]
    public class LazyTests
    {
        private static readonly Lazy<object> LazyObj = new Lazy<object>(() =>
        {
            counter = counter + 1; 
            return new object(); 
        }, true);

        private static object Property => LazyObj.Value;
        private static int counter = 0;
        
        #region LazyValue

        [Test]
        public void LazyValue__byDefault__IsSameAsBeginning()
        {
            // Arrange
            
            object o = Property;
            //Act
            var result = Property;

            //Assert
            Assert.That(result,
                Is.SameAs(o)
            );

            Assert.That(counter,
                    Is.EqualTo(1)
                );

        }

        
        [Test]
        public void LazyValue__WhenLazyValueInFunction__IsSameAsBeginning()
        {
            // Arrange

            Func<object> func = () => Property;

            var resultOfFunc = func.Invoke();
            
            //Act
            var result = Property;

            //Assert
            Assert.That(result,
                Is.SameAs(resultOfFunc)
            );

            Assert.That(counter,
                Is.EqualTo(1)
            );

            
        }

        #endregion
        
    }
}