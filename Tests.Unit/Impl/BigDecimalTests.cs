using System;
using System.Globalization;
using System.Numerics;
using NUnit.Framework;
using Sipol.Numeric.Abstracts;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class BigDecimalTests
    {
        private string _separator = null;
        private BigDecimal sut;


        [SetUp]
        public void Setup()
        {
            _separator = ",";
        }

        
        #region CreateInstance
        
        
        [Test]
        public void CreateInstance__WhenSetToInt__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            //Act
            
            var result = BigDecimal.CreateInstance((int) 10);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                    Is.Not.Empty &
                    Does.Not.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
            
            
            Assert.Pass();
        }
        
        
        [Test]
        public void CreateInstance__WhenSetToUnsignedInt__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance((uint) 10);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Not.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }

        
        [Test]
        public void CreateInstance__WhenSetToLong__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance(10L);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Not.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }

        
        [Test]
        public void CreateInstance__WhenSetToUnsignedLong__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance((ulong) 10L);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Not.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }

        
        [Test]
        public void CreateInstance__WhenSetToIntegerFloat__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance(10.0f);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Not.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }

        [Test]
        public void CreateInstance__WhenSetToNotIntegerFloat__ToStringNotNullAndNotEmptyAndContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance(10.1f);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }
        
        [Test]
        public void CreateInstance__WhenSetToIntegerDouble__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance((double)10);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Not.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }
        
        
        [Test]
        public void CreateInstance__WhenSetToNotIntegerDouble__ToStringNotNullAndNotEmptyAndContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance((double)12.345d);

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }
        
        
        [Test]
        public void CreateInstance__WhenSetBigIntegerWithoutScale__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            //Act
            var result = BigDecimal.CreateInstance(new BigInteger(10L));

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Not.Contain(_separator)
            );
            Console.WriteLine(result.ToString());
        }

        
        [Test]
        public void CreateInstance__WhenSetBigIntegerWithScale__ToStringNotNullAndNotEmptyAndNotContainsSeparator()
        {
            // Arrange
            Console.WriteLine("Start");
            //Act
            var result = BigDecimal.CreateInstance(new BigInteger(10L), 1);
            Console.WriteLine("After Act");

            //Assert
            Assert.That(result.ToString(),
                Is.Not.Null &
                Is.Not.Empty &
                Does.Contain(_separator)
            );
            Console.WriteLine("After assert");

            Console.WriteLine(result.ToString());
        }
        
        
        #endregion
        
        
        #region Scale

        [Test]
        public void Scale__byDefault__ToStringHasScaleDigitsAtEnd()
        {
            // Arrange
            sut = BigDecimal.CreateInstance(new BigInteger(Decimal.MaxValue), 4);
            //Act
            var result = sut.ToString();

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty &
                Does.Match(_separator + @"([0-9]{4})$")
            );

            Console.WriteLine(result);
            
        }

        #endregion


        #region IsOne

        [Test]
        public void IsOne__SutFromBigIntegerOneWithScale__IsFalse()
        {
            // Arrange
            sut = BigDecimal.CreateInstance(BigInteger.One, 1);
            //Act
            var result = sut.IsOne;

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void IsOne__SutFromBigIntegerOneWithoutScale__IsTrue()
        {
            // Arrange
            sut = BigDecimal.CreateInstance(BigInteger.One, 0);
            //Act
            var result = sut.IsOne;

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        #endregion

        #region ToInteger

        [Test]
        public void ToInteger__byDefault__ResultIsNotNullInstanceOfBigInteger()
        {
            // Arrange
            int value = 10;
            sut = BigDecimal.CreateInstance(value);
            //Act
            var result = sut.ToInteger();

            //Assert
            Assert.That(result,
                    Is.Not.Null &
                    Is.InstanceOf<BigInteger>()
            );
        }

        [Test]
        public void ToInteger__WhenIsIntegerValue__ResultEqualToBigIntegerValue()
        {
            // Arrange
            int value = 10;
            sut = BigDecimal.CreateInstance(value);
            //Act
            var result = sut.ToInteger();

            //Assert
            Assert.That(result,
                    Is.EqualTo(new BigInteger(value))
            );
        }

        [Test]
        public void ToInteger__WhenIsValueWithScale_ResultEqualToBigIntegerValue()
        {
            // Arrange
            float value = 10.1f;
            sut = BigDecimal.CreateInstance(value);
            //Act
            var result = sut.ToInteger();

            //Assert
            Assert.That(result,
                Is.EqualTo(new BigInteger(value))
            );
        }
        
        

        #endregion


        #region ToDecimalFraction

        [Test]
        public void ToDecimalFraction__WhenSetToFloatWithDecimalFraction__ResultIsEqualToBigIntegerDecimalFractionValue()
        {
            // Arrange
            float value = 14.123f;
            sut = BigDecimal.CreateInstance(value);
            //Act
            var result = sut.ToDecimalFraction();

            //Assert
            Assert.That(result,
                Is.EqualTo(new BigInteger(123))
            );
        }

        [TestCase("123",1)]
        [TestCase("123",3)]
        [TestCase("123",4)]
        public void ToDecimalFraction__WhenSetToFloatWithDecimalFraction__ResultIsEqualToBigIntegerDecimalFractionValue(string numstring, int scale)
        {
            // Arrange
            BigInteger value = BigInteger.Parse(numstring, NumberStyles.Integer, CultureInfo.InvariantCulture);
            //todo: repair test
            
            string fractNumString = numstring.Substring(numstring.Length - scale, scale);
            BigInteger expected = BigInteger.Parse(fractNumString, NumberStyles.Integer, CultureInfo.InvariantCulture);
            
            
            sut = BigDecimal.CreateInstance(value, scale);
            //Act
            
            
            var result = sut.ToDecimalFraction();

            //Assert

            Assert.That(result,
                Is.EqualTo(expected)
            );
        }




        #endregion

        #region BigInteger

        [Test]
        public void BigInteger__add()
        {
            BigInteger bi = new BigInteger(decimal.MaxValue);

            var result = bi + 1;

            Console.WriteLine(result.ToString());
        }
        
        [Test]
        public void BigInteger__multiply()
        {
            BigInteger bi = new BigInteger(5);

            var result = bi * 2;

            Console.WriteLine(result.ToString());

        }
        
        [Test]
        public void BigInteger__div()
        {
            BigInteger bi = new BigInteger(10);

            var result = bi / 2;

            Console.WriteLine(result.ToString());
        }
        
        [Test]
        public void BigInteger__subtract()
        {
            BigInteger bi = new BigInteger(decimal.MinValue);

            var result = bi - 1;

            Console.WriteLine(result.ToString());
        }
        
        

        #endregion
    }
}