﻿using System;
using NUnit.Framework;
using Sipol.Numeric.Abstracts;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class NumberStringRounderTests
    {
        private NumberStringRounder sut;

        [SetUp]
        public void Setup()
        {
            sut = null;
        }


        #region Round

        [Test]
        public void Round__byDefault__ResultIsNotNullInstanceOfNumberString()
        {
            // Arrange
            sut = CreateSUT(NumberString.One, 1);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>()
            );
        }

        [Test]
        public void Round__WhenNumberStringZero__ResultIsNotNullInstanceOfNumberStringAndEqualToNumberZero()
        {
            // Arrange
            sut = CreateSUT(NumberString.Zero, 0);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>() &
                Is.EqualTo(NumberString.Zero)
            );
        }

        
        [Test]
        public void Round__WhenDigitIndexIsMinusOneAndLastDigitIsZero__ResultIsSameAsNumberStringZero()
        {
            // Arrange
            const string tested = "9876543210";
            NumberString expected = NumberString.Factory.Make(tested);
            NumberString testedNumberString = NumberString.Factory.Make(tested); 
            sut = CreateSUT(testedNumberString, -1);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result,
                        Is.EqualTo(expected)
            );

            Console.WriteLine("[{0}]/{1} = [{2}]/{3}", testedNumberString.Value, testedNumberString.Value.Length, result.Value, result.Value.Length);
        }

        [Test]
        public void Round__WhenDigitIndexIsMinusOneAndLastDigitIsNine__ResultIsSameAsNumberStringZero()
        {
            // Arrange
            const string tested = "9876543219";
            NumberString expected = NumberString.Factory.Make(tested);
            NumberString testedNumberString = NumberString.Factory.Make(tested); 
            sut = CreateSUT(testedNumberString, -1);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result,
                Is.EqualTo(expected)
            );

            Console.WriteLine("[{0}]/{1} = [{2}]/{3}", testedNumberString.Value, testedNumberString.Value.Length, result.Value, result.Value.Length);
        }

        
        [Test]
        public void Round__WhenDigitNumberIsZeroAndNumberStringNotNeedRound__ResultValueStringIsEqualToNumberStringRoundedWithOneBeforeNumberString()
        {
            // Arrange
            const string tested = "1234";
            string expected = "".PadLeft(tested.Length ,'0');
            sut = CreateSUT(NumberString.Factory.Make(tested), 0);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result.Value,
                Is.EqualTo(expected)
            );

            Console.WriteLine("[{0}] = [{1}]", tested, result.Value);
        }

        [Test]
        public void Round__WhenDigitNumberIsZeroAndNumberStringNeedRound__ResultValueStringIsEqualToZerosStringWithSameLength()
        {
            // Arrange
            const string tested = "54321";
            string expected = "00000";
            sut = CreateSUT(NumberString.Factory.Make(tested), 0);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result.Value,
                Is.EqualTo(expected)
            );

            Console.WriteLine("[{0}] = [{1}]", tested, result.Value);
        }

        [Test]
        public void Round__WhenDigitNumberIsZeroAndNumberStringNeedRound__SutAdditionalOneIsTrue()
        {
            // Arrange
            const string tested = "54321";
            sut = CreateSUT(NumberString.Factory.Make(tested), 0);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(sut.IsNextDigit,
                Is.True
            );

            Console.WriteLine("[{0}] = [{1}]", tested, result.Value);
        }

        [Test]
        public void Round__WhenDigitNumberIsZeroAndNumberStringNotNeedRound__ResultValueStringIsEqualToZerosStringWithSameLength()
        {
            // Arrange
            const string tested = "12345";
            string expected = "00000";
            sut = CreateSUT(NumberString.Factory.Make(tested), 0);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result.Value,
                Is.EqualTo(expected)
            );

            Console.WriteLine("[{0}] = [{1}]", tested, result.Value);
        }

        [Test]
        public void Round__WhenDigitNumberIsZeroAndNumberStringNotNeedRound__SutAdditionalOneIsFalse()
        {
            // Arrange
            const string tested = "12345";
            sut = CreateSUT(NumberString.Factory.Make(tested), 0);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(sut.IsNextDigit,
                Is.False
            );

            Console.WriteLine("[{0}] = [{1}]", tested, result.Value);
        }

        [Test]
        public void Round__WhenDigitNumberIsZeroAndFirstDigitNine__ResultIsEqualToNumberStringIsZeroWithLengthOfTestedString()
        {
            // Arrange
            const string tested = "9876543210";
            NumberString expected = NumberStringEndingZeros.Factory.Make("0", tested.Length);
            NumberString testedNumberString = NumberString.Factory.Make(tested);
            sut = CreateSUT(testedNumberString, 0);

            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result,
                Is.EqualTo(expected)
            );


            Console.WriteLine("[{0}]/{1} = [{2}]/{3}", testedNumberString.Value, testedNumberString.Value.Length, result.Value, result.Value.Length);
        }

        [Test]
        public void Round__WhenDigitNumberIsZeroAndFirstDigitNine__SutAdditionalOneIsTrue()
        {
            // Arrange
            const string tested = "9876543210";
            NumberString expected = NumberStringEndingZeros.Factory.Make("0", tested.Length);
            NumberString testedNumberString = NumberString.Factory.Make(tested);
            sut = CreateSUT(testedNumberString, 0);

            //Act
            var result = sut.Round();

            //Assert
            Assert.That(sut.IsNextDigit,
                    Is.True
                );


            Console.WriteLine("[{0}]/{1} = [{2}]/{3}", testedNumberString.Value, testedNumberString.Value.Length, result.Value, result.Value.Length);
        }


        [TestCase("9876543210",6,"9876540000")]
        [TestCase("9876543210",5,"9876500000")]
        [TestCase("9876543210",4,"9877000000")]
        public void Round__WhenDigitNumberGreaterThanZeroAndLessThanLength__ResultValueIsEqualToExcepted(string tested, int digitNumber, string excepted)
        {
            // Arrange
            NumberString testedNumberString = NumberString.Factory.Make(tested); 
            sut = CreateSUT(testedNumberString, digitNumber);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result.Value,
                    Is.EqualTo(excepted)                
            );

            Console.WriteLine("[{0}]/{1} = [{2}]/{3}", testedNumberString.Value, testedNumberString.Value.Length, result.Value, result.Value.Length);
        }

        [TestCase("9876543210")]
        [TestCase("9876543210")]
        [TestCase("9876543210")]
        public void Round__WhenDigitNumberGreaterThanLength__ResultValueIsEqualToTested(string tested)
        {
            // Arrange
            NumberString testedNumberString = NumberString.Factory.Make(tested); 
            sut = CreateSUT(testedNumberString, tested.Length+1);
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result.Value,
                Is.EqualTo(tested)                
            );

            Console.WriteLine("[{0}]/{1} = [{2}]/{3}", testedNumberString.Value, testedNumberString.Value.Length, result.Value, result.Value.Length);
        }


        [TestCase("9876543210","0000000000", true)]
        [TestCase("98765","00000", true)]
        [TestCase("12345","00000", false)]
        public void Round__WhenDigitNumberLessThanMinusLength__ResultValueAndIsNextDigitIsEqualToExpected(string tested, string expected, bool additionalOneExcepted)
        {
            // Arrange
            NumberString testedNumberString = NumberString.Factory.Make(tested); 
            sut = CreateSUT(testedNumberString, -(tested.Length+1));
            
            //Act
            var result = sut.Round();

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result.Value,
                        Is.EqualTo(expected)                
                    );

                    Assert.That(sut.IsNextDigit,
                                Is.EqualTo(additionalOneExcepted)
                        );
                }
            );

            

            Console.WriteLine("[{0}]/{1} = [{2}]/{3}", testedNumberString.Value, testedNumberString.Value.Length, result.Value, result.Value.Length);
        }
        
        [Test] 
        public void Round__WhenDigitNumberIsSetToLastDigit__ResultIsEqualToInputString()
        {
            // Arrange
            string tested = "0123456";
            sut = CreateSUT(NumberString.Factory.Make(tested), 7);
            //Act
            var result = sut.Round();

            //Assert
            Assert.That(result.Value,
                Is.EqualTo(tested)
            );

            Console.WriteLine("{0} = {1}", tested, result);
        }

        #endregion


        #region help funcs


        private NumberStringRounder CreateSUT(NumberString number, int digitNumber)
        {
            sut = new NumberStringRounder(number, digitNumber);
            return sut;
        }
        

        #endregion
        
    }
}