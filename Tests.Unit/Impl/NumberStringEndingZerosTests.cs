﻿using NUnit.Framework;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class NumberStringEndingZerosTests
    {
        private NumberStringEndingZeros sut;

        [SetUp]
        public void Setup()
        {
            sut = null;
        }

        #region Value

        [Test]
        public void Value__byDefault__IsNotNullAndIsNotEmpty()
        {
            // Arrange
            string tested = "123";
            sut = CreateSUT(tested);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty
            );
        }
        
        [Test]
        public void Value__WhenLengthLessThanNumericStringLength__IsEqualToNumericValue()
        {
            // Arrange
            string tested = "123";
            sut = CreateSUT(tested, tested.Length-1);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(tested)
            );
        }

        [Test]
        public void Value__WhenLengthEqualToNumericStringLength__IsEqualToNumericValue()
        {
            // Arrange
            string tested = "123";
            sut = CreateSUT(tested, tested.Length );
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(tested)
            );
        }

        [TestCase(1)]
        [TestCase(3)]
        [TestCase(33)]
        public void Value__WhenLengthGreaterThanNumericStringLength__DoesMatchEndingZeros(int additionalZeros)
        {
            // Arrange
            additionalZeros = (additionalZeros < 1 ? 1 : additionalZeros);
            string tested = "123";
            sut = CreateSUT(tested, tested.Length + additionalZeros );
            
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                        Does.Match($"(0{{{additionalZeros}}})$")
            );
        }

        #endregion


        #region GetHashCode

        [Test]
        public void GetHashCode__byDefault__ResultIsNotZero()
        {
            // Arrange

            string tested = "123";
            int length = tested.Length + 5;
            sut = CreateSUT(tested, length);
            
            //Act
            var result = sut.GetHashCode();

            //Assert
            Assert.That(result,
                    Is.Not.Zero
                );
        }

        [Test]
        public void GetHashCode__byDefault__ResultIsEqualToValueHashCode()
        {
            // Arrange

            string tested = "123";
            int length = tested.Length + 5;
            sut = CreateSUT(tested, length);
            
            //Act
            var result = sut.GetHashCode();

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.Value.GetHashCode())
            );
        }
        
        #endregion


        #region Equals

        [Test]
        public void Equals__WithNull__ResultIsFalse()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(null);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        
        [Test]
        public void Equals__WithSelf__ResultIsTrue()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(sut);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        [Test]
        public void Equals__WithOtherTypeNotNullObject__ResultIsFalse()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            object with = new object();
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.False
            );
        }
        
        [Test]
        public void Equals__WithNumberStringWithSameValue__ResultIsTrue()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            NumberString with = NumberString.Factory.Make(tested);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        [Test]
        public void Equals__WithNumberStringWithOtherValue__ResultIsFalse()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            NumberString with = NumberString.Factory.Make($"{tested}4");
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void Equals__WithNumberStringEndingZerosWithSameValue__ResultIsTrue()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            NumberString with = NumberStringEndingZeros.Factory.Make(tested, length);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        [Test]
        public void Equals__WithNumberStringEndingZerosWithOtherValue__ResultIsFalse()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            NumberString with = NumberStringEndingZeros.Factory.Make($"{tested}4", length);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void Equals__StaticWithNumberStringEqualToSut__ResultIsTrue()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            NumberStringEndingZeros withNumber = NumberStringEndingZeros.Factory.Make(tested, length);
            
            //Act
            var result = NumberStringEndingZeros.Equals(sut, withNumber);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        [Test]
        public void Equals__StaticWithNumberStringEqualToSutButSmallLength__ResultIsTrue()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            NumberStringEndingZeros withNumber = NumberStringEndingZeros.Factory.Make(tested, length-1);
            
            //Act
            var result = NumberStringEndingZeros.Equals(sut, withNumber);

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__StaticWithNumberStringEqualToSutButBiggerLength__ResultIsTrue()
        {
            // Arrange
            string tested = "123";
            int length = tested.Length + 5;
            
            sut = CreateSUT(tested, length);
            NumberStringEndingZeros withNumber = NumberStringEndingZeros.Factory.Make(tested, length+1);
            
            //Act
            var result = NumberStringEndingZeros.Equals(sut, withNumber);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        
        #endregion

        #region Make

        [Test]
        public void Make__FromStringAndLength__IsNotNullAndInstanceOfNumberStringEndingZeros()
        {
            // Arrange
            const string tested = "78";
            
            //Act
            var result = NumberStringEndingZeros.Factory.Make(tested, tested.Length+3);

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberStringEndingZeros>()
            );
        }

        [Test]
        public void Make__FromNumberString__IsNotNullAndInstanceOfNumberStringEndingZeros()
        {
            // Arrange
            const string tested = "78";
            
            //Act
            var result = NumberStringEndingZeros.Factory.Make(NumberString.Factory.Make(tested), tested.Length+3);

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberStringEndingZeros>()
            );
        }

        #endregion
        
        
        #region help funcs


        private NumberStringEndingZeros CreateSUT(string numeric, int length = 0)
        {
            sut = NumberStringEndingZeros.Factory.Make(numeric, length);
            return sut;
        }
        

        #endregion

    }
}