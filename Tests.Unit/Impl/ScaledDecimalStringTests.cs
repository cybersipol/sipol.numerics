﻿using System;
using System.Globalization;
using NUnit.Framework;
using Sipol.Numeric.Abstracts;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class ScaledDecimalStringTests
    {
        private ScaledDecimalString sut;
        private string _decimalSeparator;
        
        [SetUp]
        public void Setup()
        {
            _decimalSeparator = CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator;
            sut = null;
        }

        #region Value

        [Test]
        public void Value__byDefault__IsNotNullAndNotEmpty()
        {
            // Arrange
            double tested = 1.2f;
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty
            );
        }

        [Test]
        public void Value__WhenScaleIsZeroAndFractionPartNotNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "199.2";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                        Is.EqualTo(sut.IntegerPart.Value)
                );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }

        [Test]
        public void Value__WhenScaleIsZeroAndFractionPartNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "199.6";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value)
            );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }

        [Test]
        public void Value__WhenMinusScaleIsZeroAndFractionPartNotNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "-199.2";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value)
            );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }

        [Test]
        public void Value__WhenMinusScaleIsZeroAndFractionPartNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "-199.6";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value)
            );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }

        [Test]
        public void Value__WhenScaleIsMinusAndFractionPartNotNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "123.2";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -2,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value)
            );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }

        [Test]
        public void Value__WhenMinusAndScaleIsMinusAndFractionPartNotNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "-123.2";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -2,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value)
            );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }

        [Test]
        public void Value__WhenScaleIsMinusAndFractionPartNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "199.6";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -2,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value)
            );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }

        [Test]
        public void Value__WhenMinusAndScaleIsMinusAndFractionPartNeedRound__IsEqualToIntegerPart()
        {
            // Arrange
            string tested = "-199.6";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -2,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value)
            );

            Console.WriteLine("{0} = {1}", result, sut.IntegerPart.Value);
        }
        
        [Test]
        public void Value__WhenScaleIsGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 2;
            string tested = "199.1234";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }

        [Test]
        public void Value__WhenMinusAndScaleIsGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 2;
            string tested = "-199.1234";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }
        
        [Test]
        public void Value__WhenScaleIsGreaterThanZeroAndFractionPartNeedRound__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 2;
            string tested = "199.1256";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }

        [Test]
        public void Value__WhenMinusAndScaleIsGreaterThanZeroAndFractionPartNeedRound__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 2;
            string tested = "-199.1256";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }

        
        [Test]
        public void Value__WhenScaleIsGreaterThanFractionPartLength__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "199.1234";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }

        [Test]
        public void Value__WhenMinusAndScaleIsGreaterThanFractionPartLength__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "-199.1234";
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }

        
        [Test]
        public void Value__WhenCreateFromNumberStringAndScaleGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "199";
            NumberString numberString = NumberString.Factory.Make(tested);
            sut = CreateSUT(numberString, scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }
        
        [Test]
        public void Value__WhenMinusAndCreateFromNumberStringAndScaleGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "-199";
            NumberString numberString = NumberString.Factory.Make(tested);
            sut = CreateSUT(numberString, scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }
        
        [Test]
        public void Value__WhenCreateFromSignNumberStringAndScaleGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "199";
            SignNumberString numberString = SignNumberString.Factory.Make(tested);
            sut = CreateSUT(numberString, scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }

        [Test]
        public void Value__WhenMinusAndCreateFromSignNumberStringAndScaleGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "-199";
            SignNumberString numberString = SignNumberString.Factory.Make(tested);
            sut = CreateSUT(numberString, scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }

        [Test]
        public void Value__WhenCreateFromDecimalNumberStringWithFractionPartNotZeroAndScaleGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "199.23";
            DecimalString numberString = DecimalString.Factory.Make(tested, _decimalSeparator);
            sut = CreateSUT(numberString, scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }
        
        [Test]
        public void Value__WhenMinusAndCreateFromDecimalNumberStringWithFractionPartNotZeroAndScaleGreaterThanZero__IsEqualToGluedValuesOfIntegerAndFractionPartWithDecimalSeparator()
        {
            // Arrange
            int scale = 7;
            string tested = "-199.23";
            DecimalString numberString = DecimalString.Factory.Make(tested, _decimalSeparator);
            sut = CreateSUT(numberString, scale,  _decimalSeparator);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value)
            );

            Console.WriteLine("{0} = {1} [tested: {2}]", result, sut.IntegerPart.Value + _decimalSeparator + sut.FractionPart.Value, tested);
        }
        
        #endregion

        #region IntegerPart

        [Test]
        public void IntegerPart__byDefault__ValueIsNotNullAndNotEmpty()
        {
            // Arrange
            double tested = 1.2f;
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.Not.Null &
                Is.Not.Empty
            );
        }

        [Test]
        public void IntegerPart__WhenScaleIsZeroAndFractionPartNotNeedRound__IsEqualToIntegerPartDecimalString()
        {
            // Arrange
            string tested = "299.2";
            DecimalString decimalString = DecimalString.Factory.Make(tested, _decimalSeparator);
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result,
                    Is.EqualTo(decimalString.IntegerPart)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsZeroAndFractionPartNotNeedRound__IsEqualToIntegerPartDecimalStringAndResultMinusIsTrue()
        {
            // Arrange
            string tested = "-299.2";
            DecimalString decimalString = DecimalString.Factory.Make(tested, _decimalSeparator);
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.EqualTo(decimalString.IntegerPart)
                    );

                    Assert.That(result.IsMinus,
                                Is.True
                        );
                }
            );

        }

        [Test]
        public void IntegerPart__WhenScaleIsZeroAndFractionPartNeedRound__ValueIsEqualToIntegerPartWithIncrement()
        {
            // Arrange
            string tested = "299.6";
            string expected = "300";            
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0, _decimalSeparator);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsZeroAndFractionPartNeedRound__ValueIsEqualToIntegerPartWithIncrement()
        {
            // Arrange
            string tested = "-299.6";
            string expected = "-300";            
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0, _decimalSeparator);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenScaleIsZeroAndIntegerPartIsLastNumberAndFractionPartNeedRound__ValueIsEqualToIntegerPartWithIncrement()
        {
            // Arrange
            string tested = "999.6";
            string expected = "1000";            
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0, _decimalSeparator);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsZeroAndIntegerPartIsLastNumberAndFractionPartNeedRound__ValueIsEqualToIntegerPartWithIncrement()
        {
            // Arrange
            string tested = "-999.6";
            string expected = "-1000";            
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 0, _decimalSeparator);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenScaleIsGreaterThanZeroAndIntegerPartIsLastNumberAndFractionPartIsLastNumberAndNeedRounded__ValueIsEqualToIntegerPartWithIncrement()
        {
            // Arrange
            string tested = "999.9999";
            string expected = "1000";            
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 2, _decimalSeparator);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsGreaterThanZeroAndIntegerPartIsLastNumberAndFractionPartIsLastNumberAndNeedRounded__ValueIsEqualToIntegerPartWithIncrement()
        {
            // Arrange
            string tested = "-999.9999";
            string expected = "-1000";            
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 2, _decimalSeparator);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }
        
        [Test]
        public void IntegerPart__WhenScaleGreaterThanZero__IsEqualToIntegerPartDecimalString()
        {
            // Arrange
            double tested = 299.2f;
            DecimalString decimalString = DecimalString.Factory.Make(tested);
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 5);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result,
                Is.EqualTo(decimalString.IntegerPart)
            );
        }
        
        [Test]
        public void IntegerPart__WhenMinusAndScaleGreaterThanZero__IsEqualToIntegerPartDecimalStringAndResultMinusIsTrue()
        {
            // Arrange
            double tested = -299.2f;
            DecimalString decimalString = DecimalString.Factory.Make(tested);
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 5);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.EqualTo(decimalString.IntegerPart)
                    );

                    Assert.That(result.IsMinus,
                                Is.True
                        );
                }
            );

            
        }

        [Test]
        public void IntegerPart__WhenScaleIsMinusOne__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expected = "1230";
            string tested = "1234.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -1);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }
        
        [Test]
        public void IntegerPart__WhenMinusAndScaleIsMinusOne__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expected = "-1230";
            string tested = "-1234.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -1);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenScaleIsMinusOneAndNeedRound__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expected = "1240";
            string tested = "1235.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -1);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsMinusOneAndNeedRound__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expected = "-1240";
            string tested = "-1235.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -1);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenScaleIsMinusTwo__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expected = "1200";
            string tested = "1234.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -2);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }
        
        [Test]
        public void IntegerPart__WhenMinusAndScaleIsMinusTwo__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expected = "-1200";
            string tested = "-1234.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -2);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenScaleIsMinusAndAbsScaleGreaterThanIntegerPartLengthAndNotNeedRounded__IsEqualToNumberStringZero()
        {
            // Arrange
            string expectedValue = "0000";
            string tested = "1234.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -expectedValue.Length-2);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result,
                Is.EqualTo(NumberString.Zero)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsMinusAndAbsScaleGreaterThanIntegerPartLengthAndNotNeedRounded__IsEqualToNumberStringZero()
        {
            // Arrange
            string expectedValue = "0000";
            string tested = "-1234.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -expectedValue.Length-2);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result,
                Is.EqualTo(NumberString.Zero)
            );
        }

        [Test]
        public void IntegerPart__WhenScaleIsMinusAndAbsScaleEqualToIntegerPartLengthAndNeedRounded__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expectedValue = "0000";
            string expected = $"1{expectedValue}";
            string tested = "9876.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -expectedValue.Length);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsMinusAndAbsScaleEqualToIntegerPartLengthAndNeedRounded__ValueIsEqualToIntegerPartWithRound()
        {
            // Arrange
            string expectedValue = "0000";
            string expected = $"-1{expectedValue}";
            string tested = "-9876.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -expectedValue.Length);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        [Test]
        public void IntegerPart__WhenMinusAndScaleIsMinusTwo__ValueIsEqualToIntegerPartWithRoundAndWithMinus()
        {
            // Arrange
            string expected = "-1200";
            string tested = "-1234.321";
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), -2);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expected)
            );
        }

        #endregion


        #region FractionPart

        [Test]
        public void FractionPart__byDefault__ValueIsNotNullAndNotEmpty()
        {
            // Arrange
            double tested = 299.2f;
            
            sut = CreateSUT(tested.ToString(CultureInfo.InvariantCulture), 1);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.Not.Null &
                Is.Not.Empty
            );
        }

        [Test]
        public void FractionPart__WhenScaleIsZero__IsEqualToNumberStringZeroValue()
        {
            // Arrange
            string tested = "299.2";
            
            sut = CreateSUT(tested, 0);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                    Is.EqualTo(NumberString.Zero.Value)
            );
        }
        
        [Test]
        public void FractionPart__WhenScaleIsLessThanZeroAndNotNeedRound__IsEqualToNumberStringZeroValue()
        {
            // Arrange
            string tested = "299.2";
            DecimalString decimalString = DecimalString.Factory.Make(tested);
            
            sut = CreateSUT(tested, -1);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(NumberString.Zero.Value)
            );
        }
        
        [Test]
        public void FractionPart__WhenScaleIsGreaterThanZeroAndNotNeedRound__ValueIsEqualToOnlyScaledLengthDigits()
        {
            // Arrange
            string excepted = "12";
            string tested = $"299.{excepted}34";
            
            sut = CreateSUT(tested, excepted.Length);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(excepted)
            );
        }

        [Test]
        public void FractionPart__WhenScaleIsGreaterThanZeroAndNeedRound__ValueIsEqualToOnlyScaledLengthDigitsWithRounded()
        {
            // Arrange
            string beforeRound = "11";
            string excepted = "12";
            string tested = $"299.{beforeRound}56";
            
            sut = CreateSUT(tested, beforeRound.Length);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(excepted)
            );
        }

        [Test]
        public void FractionPart__WhenScaleIsGreaterThanZeroAndRoundLastNumber__ValueIsEqualToOnlyScaledLengthDigitsWithRounded()
        {
            // Arrange
            string beforeRound = "999";
            string excepted = "000";
            string tested = $"299.{beforeRound}56";
            
            sut = CreateSUT(tested, beforeRound.Length);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(excepted)
            );
        }
        
        
        [Test]
        public void FractionPart__WhenScaleIsGreaterThanLengthOfFractionPart__ValueIsEqualToScaledLengthStringWithEndingZeros()
        {
            // Arrange
            string fraction = "1234";
            string excepted = $"{fraction}00";
            string tested = $"299.{fraction}";
            
            sut = CreateSUT(tested, fraction.Length + 2);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(excepted)
            );
            
            
        }

        #endregion
        
        #region help funcs

        private ScaledDecimalString CreateSUT(string numeric, int scale, string decimalSeparator)
        {
            sut = ScaledDecimalString.Factory.Make(numeric, scale, decimalSeparator);
            return sut;
        }

        private ScaledDecimalString CreateSUT(string numeric, int scale) => CreateSUT(numeric, scale, _decimalSeparator);

        private ScaledDecimalString CreateSUT(INumberString numeric, int scale, string decimalSeparator)
        {
            sut = ScaledDecimalString.Factory.Make(numeric, scale, decimalSeparator);
            return sut;
        }
        
        

        #endregion
    }
}