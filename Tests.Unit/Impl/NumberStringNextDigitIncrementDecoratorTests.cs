﻿using NUnit.Framework;
using Sipol.Numeric.Impl;
using NSubstitute;
using Sipol.Numeric.Abstracts;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class NumberStringNextDigitIncrementDecoratorTests
    {
        private NumberStringDigitsIncrementNextDigitDecorator sut;
        private NumberString number1;
        private INumberStringIncrementer _incrementer;
        
        [SetUp]
        public void Setup()
        {
            _incrementer = Substitute.For<INumberStringIncrementer>();
            number1 = NumberString.Factory.Make("999");
            sut = null;
        }


        #region Increment

        [Test]
        public void Increment__byDefault__ResultIsNotNullInstanceOfNumberString()
        {
            // Arrange
            _incrementer.Increment().Returns(number1);
            sut = CreateSUT();
            //Act
            var result = sut.Increment();

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>()
            );
        }
        
        [Test]
        public void Increment__byDefault__ReceivedIncrementOnIncrementer()
        {
            // Arrange
            sut = CreateSUT();
            //Act
            var result = sut.Increment();

            //Assert
            _incrementer.Received().Increment();
        }
        
        [Test]
        public void Increment__WhenIncrementerIsNextDigitEqualToTrue__ResultIsEqualToIncrementedNumberString()
        {
            // Arrange
            _incrementer.OriginalNumberString.Returns(number1);
            _incrementer.Increment().Returns(NumberString.Factory.Make("000"));
            _incrementer.IsNextDigit.Returns(true);
            sut = CreateSUT();
            //Act
            var result = sut.Increment();

            //Assert
            Assert.That(result,
                    Is.EqualTo(NumberString.Factory.Make("1000"))
            );
        }

        [Test]
        public void Increment__WhenIncrementerIsNextDigitEqualToFalse__ResultIsEqualToIncrementerNumberString()
        {
            // Arrange
            _incrementer.Increment().Returns(number1);
            _incrementer.IsNextDigit.Returns(false);
            sut = CreateSUT();
            //Act
            var result = sut.Increment();

            //Assert
            Assert.That(result,
                Is.EqualTo(number1)
            );
        }
        
        #endregion

        #region MyRegion

        private NumberStringDigitsIncrementNextDigitDecorator CreateSUT(INumberStringIncrementer inc)
        {
            sut = new NumberStringDigitsIncrementNextDigitDecorator(inc);
            return sut;
        }

        private NumberStringDigitsIncrementNextDigitDecorator CreateSUT() => CreateSUT(_incrementer);

        #endregion

    }
}