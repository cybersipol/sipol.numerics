﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Numerics;
using NUnit.Framework;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class NumberStringTests
    {
        private NumberString sut;
        private List<Type> _numberTypes;
        
        [SetUp]
        public void Setup()
        {
            _numberTypes = new List<Type>()
            {
                typeof(ulong),
                typeof(uint),
                typeof(ushort),
                typeof(byte),
                typeof(long),
                typeof(int),
                typeof(short),
                typeof(sbyte),
                typeof(decimal),
                typeof(double),
                typeof(float)
            };
        }

        #region Zero

        [Test]
        public void Zero__byDefault__IsZeroString()
        {
            // Arrange
            sut = NumberString.Zero;
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(NumberString.IsZeroString(result),
                Is.True
            );
        }

        #endregion
        
        #region One

        [Test]
        public void One__byDefault__IsOneString()
        {
            // Arrange
            sut = NumberString.One;
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(NumberString.IsOneString(result),
                Is.True
            );
        }

        #endregion


        #region IsOneString

        [TestCase(null)]
        [TestCase("")]
        [TestCase("  ")]
        [TestCase("aaa bb ccc ---- ddd")]
        public void IsOneString__WhenNotNumericString__ResultIsFalse(string tested)
        {
            // Arrange
            //Act
            var result = NumberString.IsOneString(tested);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        
        [TestCase("1")]
        [TestCase(" 1----bbbn")]
        public void IsOneString__WhenStringHasOlnyOneDigitOne__ResultIsTrue(string tested)
        {
            // Arrange
            //Act
            var result = NumberString.IsOneString(tested);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        [TestCase("11")]
        [TestCase(" 1--1--bbbn1hahah1")]
        public void IsOneString__WhenStringHasGreaterThanOneDigitOne__ResultIsFalse(string tested)
        {
            // Arrange
            //Act
            var result = NumberString.IsOneString(tested);

            //Assert
            Assert.That(result,
                Is.False
            );
        }
        #endregion
        
        
        #region IsNumericString

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void IsNumericString__WhenParamIsIsNullOrWhiteSpaceString__ResultIsFalse(string testedValue)
        {
            // Arrange
            //Act
            var result = NumberString.IsNumericString(testedValue);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [TestCase("ala ma kota")]
        [TestCase("hello")]
        [TestCase("------")]
        [TestCase("some text value")]
        public void IsNumericString__WhenParamNotContainsDigits__ResultIsFalse(string testedValue)
        {
            // Arrange
            //Act
            var result = NumberString.IsNumericString(testedValue);

            //Assert
            Assert.That(result,
                Is.False
            );
        }
        
        
        [TestCase("0071aaa89")]
        [TestCase("6671")]
        [TestCase("-61")]
        [TestCase("0.01")]
        public void IsNumericString__WhenParamContainsDigits__ResultIsTrue(string testedValue)
        {
            // Arrange
            //Act
            var result = NumberString.IsNumericString(testedValue);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        #endregion


        #region Creator.NewInstance

        [Test]
        public void CreateInstance__WhenNotNumeric__ResultSameAsZero()
        {
            // Arrange
            string tested = "aa-bb-cc-dd";
            
            NumberString zero = NumberString.Zero;
            //Act
            var result = NumberString.Factory.Make(tested);

            
            
            
            //Assert
            Assert.That(result,
                Is.SameAs(zero)
            );
        }

        [Test]
        public void CreateInstance__WhenNumeriOneString__ResultSameAsZero()
        {
            // Arrange
            string tested = 1.ToString();
            
            //Act
            var result = NumberString.Factory.Make(tested);

            //Assert
            Assert.That(result,
                Is.SameAs(NumberString.One)
            );
        }
        #endregion
        
        

        #region Value

        [TestCase("-18891,0010","188910010")]
        [TestCase("123__456","123456")]
        [TestCase("aaaa:=123-456 789","123456789")]
        public void Value__byDefault__IsNotNullAndNotEmptyAndIsEqualToExcepted(string testedValue, string excepted)
        {
            // Arrange
            sut = NumberString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                    Is.Not.Null &
                    Is.Not.Empty &
                Is.EqualTo(excepted)
            );
        }

        [Test]
        public void Value__WhenTestedNumericStringIsNull__IsEqualToNumberStringZero()
        {
            // Arrange
            string testedValue = null;
            sut = NumberString.Factory.Make(testedValue);

            string excepted = NumberString.Zero.Value;

            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(excepted)
            );
        }

        [Test]
        public void Value__WhenTestedNumericStringIsEmpty__IsEqualToNumberStringZero()
        {
            // Arrange
            string testedValue = String.Empty;
            sut = NumberString.Factory.Make(testedValue);

            string excepted = NumberString.Zero.Value;

            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(excepted)
            );
        }
        
        [TestCase("some text")]
        [TestCase("     ")]
        [TestCase("_________-______----")]
        public void Value__WhenTestedStringIsNotNumericString__IsEqualToZero(string testedValue)
        {
            // Arrange
            sut = NumberString.Factory.Make(testedValue);
            
            string excepted = NumberString.Zero.Value;
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(excepted)
            );
        }
        
        [TestCase("123")]
        [TestCase("789100")]
        [TestCase("009192")]
        public void Value__WhenNumericStringHasOnlyDigits__IsEqualInputNumericString(string testedValue)
        {
            // Arrange
            sut = NumberString.Factory.Make(testedValue);
            
            string excepted = testedValue;
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(excepted)
            );
        }
        
        #endregion


        #region IsZero

        [Test]
        public void IsZero__WhenStringHasZeros__IsTrue()
        {
            // Arrange
            sut = NumberString.Factory.Make("0000");
            //Act
            var result = sut.IsZero;

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        #endregion


        #region Equals

        [Test]
        public void Equals__WhenEqualsToOneOfNumberTypes__ForEachTypesEqualsReturnTrue()
        {
            // Arrange
            byte tested = 126;
            sut = NumberString.Factory.Make(tested);


            foreach (Type t in _numberTypes)
            {
                object converted = ConvertToType(t, tested);

                //Act
                var result = sut.Equals(converted);

                //Assert
                Assert.That(result,
                    Is.True
                );

            }
        }

        [Test]
        public void Equals__WhenEqualsToOneOfNumberTypesMinusOne__ForEachTypesEqualsReturnFalse()
        {
            // Arrange
            byte tested = 126;
            sut = NumberString.Factory.Make(tested);

            foreach (Type t in _numberTypes)
            {
                object converted = ConvertToType(t, tested - 1);

                //Act
                var result = sut.Equals(converted);

                //Assert
                Assert.That(result,
                    Is.False
                );

            }
        }

        [Test]
        public void Equals__WhenEqualToNumberStringCreatedFromTypes__ForEachNumberStringEqualsReturnTrue()
        {
            // Arrange
            byte tested = 126;
            sut = NumberString.Factory.Make(tested);


            foreach (Type t in _numberTypes)
            {
                var converted = ConvertToType(t, tested);
                var numConverted = NumberString.Factory.Make(converted.ToString());
                
                //Act
                var result = sut.Equals(numConverted);

                //Assert
                Assert.That(result,
                    Is.True
                );

            }
        }

        [Test]
        public void Equals__WhenEqualToString__ResultIsTrue()
        {
            // Arrange
            ulong tested = 1267718820000112298uL;
            sut = NumberString.Factory.Make(tested);

            //Act
            var result = sut.Equals(tested.ToString("0", CultureInfo.InvariantCulture));

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WhenEqualToNull__ResultIsFalse()
        {
            // Arrange
            ulong tested = 1267718820000112298uL;
            sut = NumberString.Factory.Make(tested);

            //Act
            var result = sut.Equals(null);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void Equals__WithSelf__ResultIsTrue()
        {
            // Arrange
            ulong tested = 1267718820000112298uL;
            sut = NumberString.Factory.Make(tested);

            //Act
            var result = sut.Equals(sut);

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WhenEqualToSameBigInteger__ResultIsTrue()
        {
            // Arrange
            ulong tested = 1267718820000112298uL;
            
            sut = NumberString.Factory.Make(tested);

            BigInteger with = new BigInteger(tested);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WhenMinusAndEqualToSameBigInteger__ResultIsTrue()
        {
            // Arrange
            long tested = -267718820000112298L;
            
            sut = NumberString.Factory.Make(tested);

            BigInteger with = new BigInteger(tested);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WhenTwoNumberStringWithSameNumberValueButOneWithLeadingZeros__ResultIsFalse()
        {
            // Arrange
            string tested = "123";
            sut = NumberString.Factory.Make(tested);

            NumberString with = NumberString.Factory.Make($"00{tested}");
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.False
            );
            
            Console.WriteLine("{0} != {1}", sut.Value, with.Value );
        }

        #endregion


        #region GetHashCode

        [Test]
        public void GetHashCode__byDefault__ResultIsNotEqualToZero()
        {
            // Arrange
            ulong tested = 1267718820000112298uL;
            sut = NumberString.Factory.Make(tested);
            
            //Act
            var result = sut.GetHashCode();

            //Assert
            Assert.That(result,
                Is.Not.EqualTo(0)
            );
        }

        [Test]
        public void GetHashCode__byDefault__ResultIsEqualToValueHasCode()
        {
            // Arrange
            ulong tested = 1267718820000112298uL;
            sut = NumberString.Factory.Make(tested);
            
            //Act
            var result = sut.GetHashCode();

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.Value.GetHashCode())
            );
        }

        #endregion


        #region ToString

        [Test]
        public void ToString__byDefault__IsEqualToValue()
        {
            // Arrange
            sut = NumberString.Factory.Make(156); 
                
            //Act
            var result = sut.ToString();

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.Value)
            );
        }

        #endregion

        #region Make

        [Test]
        public void Make__WhenMakeFromMinusBigInteger__ResultValueIsEqualToAbsoluteValueOfBigInteger()
        {
            // Arrange
            //Act
            var result = NumberString.Factory.Make(new BigInteger(-123));

            //Assert
            Assert.That(result.Value,
                Is.EqualTo("123")
            );
        }

        [Test]
        public void Make__WhenMakeFromDecimal__ResultValueIsEqualToRoundedDecimal()
        {
            // Arrange
            //Act
            var result = NumberString.Factory.Make(new decimal(1.52d));

            //Assert
            Assert.That(result.Value,
                Is.EqualTo("2")
            );
        }

        [Test]
        public void Make__WhenMakeFromMinusDecimal__ResultValueIsEqualToRoundedDecimalWithoutSing()
        {
            // Arrange
            //Act
            var result = NumberString.Factory.Make(new decimal(-1.52d));

            //Assert
            Assert.That(result.Value,
                Is.EqualTo("2")
            );
        }
        
        
                [Test]
        public void Make__WithByteValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            byte value = byte.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithSignedByteValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            sbyte value = sbyte.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }
        
        [Test]
        public void Make__WithShortValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            short value = short.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithUnsignedShortValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            ushort value = ushort.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }
        
        [Test]
        public void Make__WithIntValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            int value = int.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithUnsignedIntValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            uint value = uint.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }
        
        [Test]
        public void Make__WithLongValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            long value = long.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithUnsignedLongValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            ulong value = ulong.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithDecimalValue__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            decimal value = decimal.MaxValue;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }
        
        [Test]
        public void Make__WithBigInteger__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            BigInteger value = new BigInteger(decimal.MaxValue);
            //Act
            var result = NumberString.Factory.Make(BigInteger.Add(value, 11));

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithDouble__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            double value = 1.2f;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithFloat__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            float value = 3.6f;
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithOtherNumberString__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            NumberString value = NumberString.Factory.Make("aaa:=8912");
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
        }

        [Test]
        public void Make__WithNumberStringLeadingZeros__IsNotNullInstanceOfNumberStringAndValueEqualToValueOfNumberStringLeadingZero()
        {
            // Arrange
            NumberStringLeadingZeros value = NumberStringLeadingZeros.Factory.Make("8192", 6);
            
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
            
            Assert.That(result.Value,
                        Is.EqualTo(value.Value)
                );

            Console.WriteLine("Value: [{0}]",result.Value);
        }

        [Test]
        public void Make__WithNumberStringEndingZeros__IsNotNullInstanceOfNumberStringAndValueEqualToValueOfNumberStringEndingZero()
        {
            // Arrange
            NumberStringEndingZeros value = NumberStringEndingZeros.Factory.Make("8192", 6);
            
            //Act
            var result = NumberString.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberString(result);
            
            Assert.That(result.Value,
                Is.EqualTo(value.Value)
            );

            Console.WriteLine("Value: [{0}]",result.Value);
        }

        #endregion
        
        
        #region help funcs

        internal static object ConvertToType(Type t, int value)
        {
            return Convert.ChangeType(value, t);
        }

        internal static T ConvertToType<T>(object value)
        {
            return (T) Convert.ChangeType(value, typeof(T));
        }
        
        
        private void AssertNotNullInstanceOfNumberString(object result) => Assert.That(result,
            Is.Not.Null &
            Is.InstanceOf<NumberString>()
        );

        

        #endregion
        
    }
}