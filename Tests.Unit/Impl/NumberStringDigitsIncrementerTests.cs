﻿using System;
using NUnit.Framework;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class NumberStringDigitsIncrementerTests
    {
        private NumberStringDigitsIncrementer sut;
        private NumberString number1;
        private NumberString number1incremented;
        private NumberString number2;
        private NumberString number2incremented;
        private SignNumberString number3;
        private NumberString number3inc;
        
        [SetUp]
        public void Setup()
        {
            number1 = NumberString.Factory.Make("12345");
            number1incremented = NumberString.Factory.Make("12346");
            number2 = NumberString.Factory.Make("99999");            
            number2incremented = NumberString.Factory.Make("00000");

            number3 = SignNumberString.Factory.Make("-1234");
            number3inc = NumberString.Factory.Make("1235");
            
            sut = null;
        }

        #region Increment

        [Test]
        public void Increment__byDefault__ResultIsNotNullInstanceOfNumberString()
        {
            // Arrange
            sut = CreateSUT(number1);
            
            //Act
            var result = sut.Increment();
            
            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>()
            );
        }
        
        [Test]
        public void Increment__byDefault__ResultEqualToNumberStringIncremented()
        {
            // Arrange
            sut = CreateSUT(number1);
            
            //Act
            var result = sut.Increment();
            
            //Assert
            Assert.That(result,
                    Is.EqualTo(number1incremented)
            );
        }
        
        [Test]
        public void Increment__WhenNumberStringIsMaxForSelfLength__ResultEqualToNumberStringIncremented()
        {
            // Arrange
            sut = CreateSUT(number2);
            
            //Act
            var result = sut.Increment();
            
            //Assert
            Assert.That(result,
                Is.EqualTo(number2incremented)
            );
        }

        [Test]
        public void Increment__WhenNumberStringIsMaxForSelfLength__SutIsNextDigitEqualToTrue()
        {
            // Arrange
            sut = CreateSUT(number2);
            
            //Act
            var result = sut.Increment();
            
            //Assert
            Assert.That(sut.IsNextDigit,
                Is.True
            );
        }

        [Test]
        public void Increment__WhenNumberStringIsMinus__ResultIsEqualToAbsoluteValueIncremented()
        {
            // Arrange
            sut = CreateSUT(number3);
            
            //Act
            var result = sut.Increment();
            
            //Assert
            Assert.That(result,
                        Is.EqualTo(number3inc)
                );
            
            
            Console.WriteLine("{0} = {1}", number3.Value, result.Value);
        }

        #endregion
        
        
        #region help funcs

        private NumberStringDigitsIncrementer CreateSUT(NumberString numberString)
        {
            sut = new NumberStringDigitsIncrementer(numberString);
            return sut;
        }
        

        #endregion

    }
}