﻿using System;
using System.Globalization;
using NUnit.Framework;
using Sipol.Numeric.Abstracts;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class DecimalStringTests
    {

        private DecimalString sut;
        private string _decimalSeparator;
        private readonly Lazy<CultureInfo> _lazyCultureInfo = new Lazy<CultureInfo>(() =>new CultureInfo(CultureInfo.InvariantCulture.LCID));
        private CultureInfo CultureInfo => _lazyCultureInfo.Value;


        [OneTimeSetUp]
        public void Start()
        {
            _decimalSeparator = ",";
            CultureInfo.NumberFormat.NumberDecimalSeparator = _decimalSeparator;
            DecimalString.Factory.CultureInfo = CultureInfo;
        }
        

        #region HasDecimalSeparator

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("ala ma kota")]
        [TestCase("-some-other-text,aaa")]
        public void HasDecimalSeparator__WhenParamIsNotNumericString__IsFalse(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);

            //Act
            var result = sut.HasDecimalSeparator;

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [TestCase("12")]
        [TestCase("188")]
        [TestCase(" -1111")]
        [TestCase("15a - 16a")]
        [TestCase("-1some-other-text:aaa")]
        public void HasDecimalSeparator__WhenParamIsNumericStringWithoutDecimalPoint__IsFalse(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            //Act
            var result = sut.HasDecimalSeparator;

            //Assert
            Assert.That(result,
                Is.False
            );
        }
        
        [TestCase("12,1")]
        [TestCase("188,00")]
        [TestCase("-881,90")]
        [TestCase("15a - 16,2a")]
        [TestCase("-12,aaa9919")]
        public void HasDecimalSeparator__WhenParamIsNumericStringWithDecimalPoint__ResultIsTrue(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            //Act
            var result = sut.HasDecimalSeparator;

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        #endregion


        #region IntegerPart

        [TestCase("123")]
        [TestCase(" 123")]
        [TestCase("ble:=123")]
        [TestCase("123   ")]
        [TestCase("123__123")]
        public void IntegerPart__WhenParamWithoutDecimalPoint__ValueDoesMatchToNumericString(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                    Does.Match(@"^([\\-]*)([0-9]+)$")
                );
            
        }

        [TestCase("123,789","123")]
        [TestCase(" 123,01","123")]
        [TestCase("ble:=123, aaa 121", "123")]
        [TestCase("123   ,","123")]
        [TestCase("123__123,00","123123")]
        [TestCase("789,","789")]
        public void IntegerPart__WhenParamWithDecimalPoint__ValueIsEqualToExpectedValue(string testedValue, string expectedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expectedValue)
            );
            
        }

        [TestCase("123,789,012","123789")]
        [TestCase(" 123,01,122","12301")]
        [TestCase("ble:=123, aaa 121, bbb 1233 ", "123121")]
        [TestCase("123   ,,912","123")]
        [TestCase("123__123,00,01","12312300")]
        [TestCase("789,12,13","78912")]
        public void IntegerPart__WhenParamWithTwoDecimalPoint__ValueIsEqualToExpectedValue(string testedValue, string expectedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expectedValue)
            );
            
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("ala ma kota")]
        [TestCase("-some-other-text,aaa")]
        public void IntegerPart__WhenParamIsNotNumericValue__IsEqualToZeroIntegerPart(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            
            //Act
            var result = sut.IntegerPart;

            //Assert
            Assert.That(result,
                Is.EqualTo(DecimalString.Zero.IntegerPart)
            );
            
        }
        
        
        #endregion


        #region FractionPart

        [TestCase("123")]
        [TestCase(" 123")]
        [TestCase("ble:=123")]
        [TestCase("123   ")]
        [TestCase("123__123")]
        public void FractionPart__WhenParamWithoutDecimalPoint__ValueIsEqualToNumberStringZeroValue(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);

            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(NumberString.Zero.Value)
            );
            
        }

        [TestCase("123,789","789")]
        [TestCase(" 123,01","01")]
        [TestCase("ble:=123, aaa 121", "121")]
        [TestCase("123__123,00","0")]
        [TestCase("789,1a1","11")]
        public void FractionPart__WhenParamWithDecimalPoint__ValueIsEqualToExpectedValue(string testedValue, string expectedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expectedValue)
            );
            
        }

        [TestCase("123,789,677","677")]
        [TestCase(" 123,01,56","56")]
        [TestCase("ble:=123, aaa 121, bbb 334", "334")]
        [TestCase("123__123,00,001","001")]
        [TestCase("789,1a1,2b2 aa","22")]
        public void FractionPart__WhenParamWithTwoDecimalPoint__ValueIsEqualToLastFractionPartExpectedValue(string testedValue, string expectedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(expectedValue)
            );
            
        }

        
        [TestCase("123,")]
        [TestCase(" 123,")]
        [TestCase("ble:=123,")]
        [TestCase("123__123,")]
        public void FractionPart__WhenParamWithDecimalPointAtEnd__ValueIsEqualToNumberStringZeroValue(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(NumberString.Zero.Value)
            );
            
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("ala ma kota")]
        [TestCase("-some-other-text,aaa")]
        public void FractionPart__WhenParamIsNotNumericValue__ValueIsEqualToNumberStringZeroValue(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            
            //Act
            var result = sut.FractionPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(NumberString.Zero.Value)
            );
            
        }

        #endregion

        #region Value

        [TestCase("123","123")]
        [TestCase(" 123","123")]
        [TestCase("ble:=123","123")]
        [TestCase("123   ","123")]
        [TestCase("123__123","123123")]
        public void Value__WhenStringIsNumericAndWithoutMinusAndWithoutDecimalPoint__IsEqualToExpected(string testedValue, string expectedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(expectedValue)
            );
        }

        [TestCase("123-","123")]
        [TestCase(" -123","-123")]
        [TestCase("ble:=-123","-123")]
        [TestCase("123   -- aaa --","123")]
        [TestCase("-123-123","-123123")]
        [TestCase("+123","123")]
        public void Value__WhenStringIsNumericWithSignBeforeFirstDigitAndWithoutDecimalSeparator__IsEqualToExpected(string testedValue, string expectedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(expectedValue)
            );
        }
        
        
        [TestCase("123,01","123,01")]
        [TestCase(" 123,01","123,01")]
        [TestCase("ble:=123,32","123,32")]
        [TestCase("123   ,98","123,98")]
        [TestCase("123__123,  456","123123,456")]
        public void Value__WhenStringIsNumericAndWithoutSignAndWithDecimalSeparator__IsEqualToExpected(string testedValue, string expectedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(expectedValue)
            );
        }

        [TestCase("-123,01","-123,01")]
        [TestCase(" -123,01","-123,01")]
        [TestCase("ble:=123,32-","123,32")]
        [TestCase("123   ,-98","123,98")]
        [TestCase("aaa:=-123__123,  456","-123123,456")]
        public void Value__WhenStringIsNumericAndWithSingAndWithDecimalSepartor__IsEqualToExpected(string testedValue, string expectedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(expectedValue)
            );
        }

        [TestCase("-123,","-123,0")]
        [TestCase(" -123,","-123,0")]
        [TestCase("ble:=12332-,","12332,0")]
        [TestCase("123   ,","123,0")]
        [TestCase("aaa:=-123__123,","-123123,0")]
        public void Value__WhenStringIsNumericWithDecimalSeparatorAtLastPosition__IsEqualToExpected(string testedValue, string expectedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(expectedValue)
            );
        }


        [TestCase("-123,123,01","-123123,01")]
        [TestCase(" -123,001,02","-123001,02")]
        [TestCase("ble:=12332-,12,11","1233212,11")]
        [TestCase("123   , 123,998","123123,998")]
        [TestCase("aaa:=-123__123,0,1","-1231230,1")]
        public void Value__WhenStringIsNumericWithTwoDecimalSeparator__ResultIsEqualToExpected(string testedValue, string expectedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(expectedValue)
            );
        }
        
        [TestCase("-123,123,","-123123,0")]
        [TestCase(" -123,001,","-123001,0")]
        [TestCase("ble:=12332-,12,","1233212,0")]
        [TestCase("123   , 123,","123123,0")]
        [TestCase("aaa:=-123__123,0,","-1231230,0")]
        public void Value__WhenStringIsNumericWithTwoDecimalPointSecondAtLastPosition__ResultIsEqualToExpected(string testedValue, string expectedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(expectedValue)
            );
        }
        
        [TestCase("aaaa")]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("     ")]
        [TestCase("some text without digits")]
        [TestCase("-other-some-text-without-digits-")]
        public void Value__WhenStringIsNotNumeric__IsEqualToDecimalStringZeroValue(string testedValue)
        {
            // Arrange
            sut = DecimalString.Factory.Make(testedValue);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(DecimalString.Zero.Value)
            );
        }

        
        
        #endregion


        #region IsMinus

        [Test]
        public void IsMinus__WhenIntegerPartIsZeroButAllNumberIsMinus__IsTrue()
        {
            // Arrange
            sut = CreateSUT($"-0{_decimalSeparator},01");
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        #endregion


        #region AbsoluteIntegerPart

        [Test]
        public void AbsoluteIntegerPart__byDefault__IsEqualToAbsoluteIntegerValueFromTestedValue()
        {
            // Arrange
            string absoluteIntegerPart = "12";
            
            string tested = $"-{absoluteIntegerPart},5";
            sut = CreateSUT(tested);
            
            //Act
            var result = sut.AbsoluteIntegerPart;

            //Assert
            Assert.That(result?.Value,
                Is.EqualTo(absoluteIntegerPart)
            );
        }

        #endregion

        #region ToString

        [TestCase("tested value")]
        [TestCase("0,00")]
        public void ToString__byDefault__ResultIsNotNullAndNotEmpty(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            //Act
            var result = sut.ToString();

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty
            );
        }
        
        [TestCase("889")]
        [TestCase("0,00")]
        [TestCase("-0,01")]
        public void ToString__byDefault__ResultDoesMatchOnlyForDigitsAndDecimalSeparatorAndSigns(string testedValue)
        {
            // Arrange
            sut = CreateSUT(testedValue);
            //Act
            var result = sut.ToString();

            //Assert
            Assert.That(result,
                    Does.Match($@"^([0-9\\{SignNumberString.PlusText}\\{SignNumberString.MinusText}\\{_decimalSeparator}]+)$")
            );
        }
        
        [Test]
        public void ToString__WhenFractionIsStringWithZeros__ResultIsEqualToIntegerValue()
        {
            // Arrange
            const string integerValue = "123";
            string testedValue = integerValue + $"{_decimalSeparator}0000";
            sut = CreateSUT(testedValue);
            //Act
            var result = sut.ToString();

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(sut.IntegerPart.Value,
                        Is.EqualTo(integerValue)
                    );

                    Assert.That(result,
                        Is.EqualTo(integerValue)
                    );
                }
            );
            
        }


        
        #endregion



        #region DecimalSeparator

        [Test]
        public void DecimalSeparator__byDefault__IsEqualToInputConstructDecimalSeparator()
        {
            // Arrange
            NumberString numberString = NumberString.Factory.Make("9345");
            
            sut = CreateSUT(numberString, _decimalSeparator);
            //Act
            var result = sut.DecimalSeparator;

            //Assert
            Assert.That(result,
                Is.EqualTo(_decimalSeparator)
            );
        }

        [Test]
        public void DecimalSeparator__WhenSetToNull__IsNotNullAndNotEmpty()
        {
            // Arrange
            NumberString numberString = NumberString.Factory.Make("9345");
            
            sut = CreateSUT(numberString, null);
            //Act
            var result = sut.DecimalSeparator;

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty
            );
        }

        #endregion

        #region CultureInfo

        [Test]
        public void CultureInfo__byDefault__IsNotNullAndSameAsInDecimalStringFactory()
        {
            // Arrange
            NumberString numberString = NumberString.Factory.Make("9345");
            sut = CreateSUT(numberString);
            
            
            //Act
            var result = sut.CultureInfo;

            //Assert
            
            Assert.That(result,
                Is.Not.Null &
                Is.SameAs(DecimalString.Factory.CultureInfo)
            );
        }

        [Test]
        public void CultureInfo__WhenSetToNull__IsNotNullAndSameAsInDecimalStringFactory()
        {
            // Arrange
            NumberString numberString = NumberString.Factory.Make("9345");
            sut = CreateSUT(numberString);
            sut.CultureInfo = null;
            
            //Act
            var result = sut.CultureInfo;

            //Assert
            
            Assert.That(result,
                Is.Not.Null &
                Is.SameAs(DecimalString.Factory.CultureInfo)
            );
        }

        [Test]
        public void CultureInfo__WhenSetToOtherCultureInfo__IsSameAsOtherCultureInfo()
        {
            // Arrange
            NumberString numberString = NumberString.Factory.Make("9345");
            sut = CreateSUT(numberString);
            CultureInfo ci = CultureInfo.CreateSpecificCulture("pl-PL");
            sut.CultureInfo = ci;
            
            //Act
            var result = sut.CultureInfo;

            //Assert
            Assert.That(result,
                Is.SameAs(ci)
            );
        }

        #endregion
        
        #region help funcs

        private DecimalString CreateSUT(string numericString, string decimalSeparator)
        {
            sut = DecimalString.Factory.Make(numericString, decimalSeparator);
            return sut;
        }

        private DecimalString CreateSUT(string numericString) => CreateSUT(numericString, _decimalSeparator);

        private DecimalString CreateSUT(INumberString numeric, string decimalSeparator)
        {
            sut = DecimalString.Factory.Make(numeric, decimalSeparator);
            return sut;
        }
        
        private DecimalString CreateSUT(INumberString numeric) => CreateSUT(numeric, _decimalSeparator);
        
        
        #endregion
    }
}