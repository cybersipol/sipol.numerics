﻿using System;
using System.Globalization;
using System.Numerics;
using NUnit.Framework;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class NumberStringLeadingZerosTests
    {
        private NumberStringLeadingZeros sut;

        [SetUp]
        public void Setup()
        {
            sut = null;
        }

        #region Value

        [Test]
        public void Value__byDefault__IsNotNullAndIsNotEmpty()
        {
            // Arrange
            const string numeric = "123";
            sut = CreateSUT(numeric);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty
            );
        }

        [Test]
        public void Value__WhenLengthIsZero__IsEqualToInputNumericString()
        {
            // Arrange
            const string numeric = "123";
            sut = CreateSUT(numeric, 0);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(numeric)
            );
        }

        [Test]
        public void Value__WhenLengthLessThanLengthOfNumericString__IsEqualToInputNumericString()
        {
            // Arrange
            const string numeric = "123";
            sut = CreateSUT(numeric, numeric.Length-1);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(numeric)
            );
        }

        [Test]
        public void Value__WhenLengthSameAsLengthOfNumericString__IsEqualToInputNumericString()
        {
            // Arrange
            const string numeric = "123";
            sut = CreateSUT(numeric, numeric.Length);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(numeric)
            );
        }

        
        [TestCase(1)]
        [TestCase(5)]
        public void Value__WhenLengthGreaterThanLengthOfNumericString__StartAdditionalLengthWithZeros(int additionalLength)
        {
            // Arrange
            additionalLength = additionalLength < 1 ? 1 : additionalLength;
            
            const string numeric = "123";
            sut = CreateSUT(numeric, numeric.Length + additionalLength);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Does.Match($"^(0{{{additionalLength}}})")
            );
        }

        
        #endregion


        #region ToString

        [Test]
        public void ToString__byDefault__IsEqualToSutValue()
        {
            // Arrange
            sut = CreateSUT("123", 5);
            //Act
            var result = sut.ToString();

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.Value)
            );
            
            Console.WriteLine(result);
        }

        #endregion


        #region GetHashCode

        [Test]
        public void GetHashCode__byDefault__ResultIsNotZero()
        {
            // Arrange
            sut = CreateSUT("123", 5);
            //Act
            var result = sut.GetHashCode();

            //Assert
            
            Assert.That(result,
                Is.Not.EqualTo(Is.Zero)
            );
            
            
        }

        [Test]
        public void GetHashCode__byDefault__ResultIsEqualToValueHashCode()
        {
            // Arrange
            sut = CreateSUT("123", 5);
            //Act
            var result = sut.GetHashCode();

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.Value.GetHashCode())
            );
            
        }
        
        
        #endregion


        #region Equals

        [Test]
        public void Equals__WhenEqualWithSelf__IsTrue()
        {
            // Arrange
            string tested = "123";
            int length = 5;
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(sut);

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WhenEqualWithNull__IsFalse()
        {
            // Arrange
            string tested = "123";
            int length = 5;
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(null);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void Equals__WhenEqualWithSameNumberString__IsTrue()
        {
            // Arrange
            string tested = "123";
            int length = 5;
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(NumberString.Factory.Make(tested));

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        [Test]
        public void Equals__WhenEqualWithSameNumberStringLeadingZero__IsTrue()
        {
            // Arrange
            string tested = "123";
            int length = 5;
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(NumberStringLeadingZeros.Factory.Make(tested, length));

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        [Test]
        public void Equals__WhenEqualWithSameNumberStringLeadingZeroWithDifferencesLengths__IsTrue()
        {
            // Arrange
            string tested = "123";
            int length = 5;
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(NumberStringLeadingZeros.Factory.Make(tested, length+4));

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WhenEqualWithSameInputString__IsTrue()
        {
            // Arrange
            string tested = "123";
            int length = 5;
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(tested);

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WhenEqualWithNotNullObject__IsFalse()
        {
            // Arrange
            string tested = "123";
            int length = 5;
            object obj = new object();
            sut = CreateSUT(tested, length);
            //Act
            var result = sut.Equals(obj);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        
        #endregion


        #region Make

        [Test]
        public void Make__WithByteValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            byte value = byte.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make__WithSignedByteValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            sbyte value = sbyte.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make__WithShortValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            short value = short.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make__WithUnsignedShortValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            ushort value = ushort.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make__WithIntValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            int value = int.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make__WithUnsignedIntValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            uint value = uint.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make__WithLongValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            long value = long.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make__WithUnsignedLongValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            ulong value = ulong.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make__WithDecimalValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            decimal value = decimal.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value, 5);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make__WithBigInteger__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            BigInteger value = new BigInteger(decimal.MaxValue);
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(BigInteger.Add(value, 11), 86);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        #endregion


        #region Make (without length)

        [Test]
        public void Make_WithoutLength__WithByteValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            byte value = byte.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithSignedByteValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            sbyte value = sbyte.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make_WithoutLength__WithShortValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            short value = short.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithUnsignedShortValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            ushort value = ushort.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make_WithoutLength__WithIntValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            int value = int.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithUnsignedIntValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            uint value = uint.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make_WithoutLength__WithLongValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            long value = long.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithUnsignedLongValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            ulong value = ulong.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithDecimalValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            decimal value = decimal.MaxValue;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }
        
        [Test]
        public void Make_WithoutLength__WithDoubleValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            double value = 1.49f;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithFloatValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            float value = 1.51f;
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithNumericStringValue__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            string value = 1.51f.ToString(CultureInfo.CurrentUICulture);
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithBigInteger__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            BigInteger value = new BigInteger(decimal.MaxValue);
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(BigInteger.Add(value, 11));

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        [Test]
        public void Make_WithoutLength__WithNumberString__IsNotNullInstanceOfNumberStringLeadingZeros()
        {
            // Arrange
            NumberString value = NumberString.Factory.Make("128");
            //Act
            var result = NumberStringLeadingZeros.Factory.Make(value);

            //Assert
            AssertNotNullInstanceOfNumberStringLeadingZeros(result);
        }

        #endregion
        
        #region help funcs

        private NumberStringLeadingZeros CreateSUT(string numeric, int length = 0)
        {
            sut = NumberStringLeadingZeros.Factory.Make(numeric, length);
            return sut;
        }
        
        private void AssertNotNullInstanceOfNumberStringLeadingZeros(object result) => Assert.That(result,
                    Is.Not.Null &
                    Is.InstanceOf<NumberStringLeadingZeros>()
            );

        

        #endregion
    }
}