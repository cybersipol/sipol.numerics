﻿using System;
using NUnit.Framework;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Impl
{
    [TestFixture]
    public class SignNumberStringTests
    {
        private SignNumberString sut;
        private string ZeroString => SignNumberString.Zero.Value;

        [SetUp]
        public void Setup()
        {
        }

        #region IsMinus

        [TestCase("123")]
        [TestCase("67 177 89")]
        public void IsMinus__WhenTestedStringWithoutMinus__IsFalse(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                    Is.False
            );
        }

        [TestCase("-123")]
        [TestCase("  - aaa 67-177 89")]
        public void IsMinus__WhenTestedStringWithMinusBeforeFirstDigit__IsTrue(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [TestCase("1-23")]
        [TestCase(" aaa 67-177 89")]
        public void IsMinus__WhenTestedStringWithMinusAfterFirstDigit__IsFalse(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [TestCase("1-23+661")]
        [TestCase(" aaa 67-177+89")]
        public void IsMinus__WhenSignsAfterFirstDigit__IsFalse(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [TestCase("+123661")]
        [TestCase(" bbb+aaa 6717789")]
        public void IsMinus__WhenPlusBeforeFirstDigit__IsFalse(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [TestCase("-+123661")]
        [TestCase(" -bbb-+aaa-----+6717789")]
        public void IsMinus__WhenLastSignBeforeFirstDigitIsPlus__IsFalse(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.False
            );
        }
        
        [TestCase("+-123661")]
        [TestCase(" +bbb-aaa 6717789")]
        public void IsMinus__WhenLastSignBeforeFirstDigitIsMinus__IsTrue(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.True
            );
        }


        [TestCase("0-0-0")]
        [TestCase("671-778-99-89")]
        public void IsMinus__WhenSignBetweenDigit__IsFalse(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [TestCase("001--")]
        [TestCase("6717789989+")]
        public void IsMinus__WhenSignAtLast__IsFalse(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.IsMinus;

            //Assert
            Assert.That(result,
                Is.False
            );
        }
        
        #endregion

        #region Value

        [TestCase("123","123")]
        [TestCase("-123","-123")]
        [TestCase("-12--3","-123")]
        [TestCase("   +12--3","123")]
        [TestCase("   +12 aaa 3 nnn ccc 0","1230")]
        [TestCase(" ----+12 aaa 3 nnn ccc 0","1230")]
        public void Value__byDefault__Expected(string tested, string expected)
        {
            // Arrange
            sut = CreateSUT(tested);
            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty &
                Is.EqualTo(expected)
            );
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("    ")]
        [TestCase("  some text + other some text - text")]
        public void Value__WhenTestedStringNotNumericString__IsEqualToNumberStringZero(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);

            //Act
            var result = sut.Value;
            
            //Assert
            Assert.That(result,
                Is.EqualTo(ZeroString)
            );
        }

        [TestCase("-0")]
        [TestCase("+0")]
        [TestCase("0-")]
        [TestCase("0+")]
        [TestCase("-0+")]
        [TestCase("+0-")]
        public void Value__WhenTestedStringIsNumericZeroStringWithSign__IsEqualToNumberStringZero(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);

            //Act
            var result = sut.Value;
            
            //Assert
            Assert.That(result,
                Is.EqualTo(ZeroString)
            );
        }

        [TestCase("0")]
        [TestCase("00")]
        [TestCase("0000  00")]
        public void Value__WhenTestedStringIsNumericMulticharZeroString__DoesMatchOnlyZeros(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);

            //Act
            var result = sut.Value;
            
            //Assert
            Assert.That(result,
                    Is.Not.Null &
                    Is.Not.Empty &
                    Does.Match("^("+ZeroString+"+)$")
            );
        }

        [TestCase("-0")]
        [TestCase("+00-")]
        [TestCase("00--00-+00 aaa")]
        public void Value__WhenTestedStringIsNumericMulticharZeroStringWithSings__DoesMatchOnlyZeros(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);

            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty &
                Does.Match("^(" + ZeroString + "+)$")
            );
        }
        
        [TestCase("123")]
        [TestCase("00123")]
        [TestCase("0012300")]
        public void Value__WhenTestedStringHasOnlyDigits__IsEqualToTestedString(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);

            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(tested)
            );
        }

        [TestCase("-123")]
        [TestCase("-00123")]
        [TestCase("-0012300")]
        public void Value__WhenTestedStringHasFirstMinusAndOnlyDigits__IsEqualToTestedString(string tested)
        {
            // Arrange
            sut = CreateSUT(tested);

            //Act
            var result = sut.Value;

            //Assert
            Assert.That(result,
                Is.EqualTo(tested)
            );
        }

        #endregion


        #region Make

        [Test]
        public void Make__WithMinusNumberString__ResultIsNotNullInstanceOfSingNumberStringAndEqualToMinusInputNumericString()
        {
            // Arrange
            const string tested = "-12";

            //Act
            var result = SignNumberString.Factory.Make(NumberString.Factory.Make(tested));

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<SignNumberString>()
                    );

                    Assert.That(result.Value,
                                    Is.EqualTo(tested)
                        );
                }
            );
    
            
        }

        [Test]
        public void Make__WithMinusNumberStringLeadingZeros__ResultIsNotNullInstanceOfSingNumberStringAndEqualToMinusInputNumericStringWithLeadingZeros()
        {
            // Arrange
            const string tested = "-12";
            NumberStringLeadingZeros leadingZeros = NumberStringLeadingZeros.Factory.Make(tested, 4);

            //Act
            var result = SignNumberString.Factory.Make(leadingZeros);

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<SignNumberString>()
                    );

                    Assert.That(result.Value,
                        Is.EqualTo(string.Format("{0}{1}", SignNumberString.MinusText, leadingZeros.Value))
                    );
                }
            );

            Console.WriteLine("Value: [{0}]", result?.Value ?? "(null)");
            
        }

        [Test]
        public void Make__WithMinusNumberStringEndingZeros__ResultIsNotNullInstanceOfSingNumberStringAndEqualToMinusInputNumericStringWithEndingZeros()
        {
            // Arrange
            const string tested = "-12";
            NumberStringEndingZeros zeros = NumberStringEndingZeros.Factory.Make(tested, 4);

            //Act
            var result = SignNumberString.Factory.Make(zeros);

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<SignNumberString>()
                    );

                    Assert.That(result.Value,
                        Is.EqualTo(string.Format("{0}{1}", SignNumberString.MinusText, zeros.Value))
                    );
                }
            );

            Console.WriteLine("Value: [{0}]", result?.Value ?? "(null)");
            
        }

        [Test]
        public void Make__WithOneNumericString__ResultIsNotNullInstanceOfSingNumberStringAndSameAsSingNumericStringOne()
        {
            // Arrange

            //Act
            var result = SignNumberString.Factory.Make(NumberString.One);

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<SignNumberString>()
                    );

                    Assert.That(result,
                        Is.SameAs(SignNumberString.One)
                    );
                }
            );
    
            
        }

        [Test]
        public void Make__WithMinusOneNumericString__ResultIsNotNullInstanceOfSingNumberStringAndSameAsSingNumericStringMinusOne()
        {
            // Arrange

            //Act
            var result = SignNumberString.Factory.Make("-" + NumberString.One);

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<SignNumberString>()
                    );

                    Assert.That(result,
                        Is.SameAs(SignNumberString.MinusOne)
                    );
                }
            );
    
            
        }
        
        [Test]
        public void Make__WithZeroNumericString__ResultIsNotNullInstanceOfSingNumberStringAndSameAsSingNumericStringZero()
        {
            // Arrange
            
            //Act
            var result = SignNumberString.Factory.Make(NumberString.Zero);

            //Assert
            Assert.Multiple(() =>
                {
                    Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<SignNumberString>()
                    );

                    Assert.That(result,
                        Is.SameAs(SignNumberString.Zero)
                    );
                }
            );
    
            
        }
        
        #endregion

        #region GetHashCode

        [Test]
        public void GetHashCode__byDefault__ResultIsNotZero()
        {
            // Arrange
            sut = CreateSUT("-12");
            //Act
            var result = sut.GetHashCode();

            //Assert
            Assert.That(result,
                Is.Not.Zero
            );
        }

        [Test]
        public void GetHashCode__byDefault__ResultIsEqualToSutValueHashCodeW()
        {
            // Arrange
            string tested = "  -12";
            sut = CreateSUT(tested);
            //Act
            var result = sut.GetHashCode();

            //Assert
            Assert.That(result,
                Is.EqualTo(sut.Value.GetHashCode())
            );
        }

        #endregion

        #region Equals

        [Test]
        public void Equals__WithNull__ResultIsFalse()
        {
            // Arrange
            string tested = "-12";
            sut = CreateSUT(tested);
            //Act
            var result = sut.Equals(null);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void Equals__WithSelf__ResultIsTrue()
        {
            // Arrange
            string tested = "-12";
            sut = CreateSUT(tested);
            //Act
            var result = sut.Equals(sut);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        
        
        [Test]
        public void Equals__WithOtherSingNumberString__ResultIsFalse()
        {
            // Arrange
            string tested = "-12";
            sut = CreateSUT(tested);
            
            SignNumberString with = SignNumberString.MinusOne;
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void Equals__WithOtherSingNumberStringWithSameValue__ResultIsTrue()
        {
            // Arrange
            string tested = "-12";
            sut = CreateSUT(tested);
            
            SignNumberString with = SignNumberString.Factory.Make(tested);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.True
            );
        }

        [Test]
        public void Equals__WithNumberStringNotEqual__ResultIsFalse()
        {
            // Arrange
            string tested = "-12";
            sut = CreateSUT(tested);
            
            NumberString with = NumberString.Factory.Make(tested);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.False
            );
        }

        [Test]
        public void Equals__WithNumberStringEqual__ResultIsTrue()
        {
            // Arrange
            string tested = "12";
            sut = CreateSUT(tested);
            
            NumberString with = NumberString.Factory.Make(tested);
            
            //Act
            var result = sut.Equals(with);

            //Assert
            Assert.That(result,
                Is.True
            );
        }
        #endregion
        

        #region help funcs

        private SignNumberString CreateSUT(string numeric)
        {
            sut = SignNumberString.Factory.Make(numeric);
            return sut;
        }
        

        #endregion
    }
}