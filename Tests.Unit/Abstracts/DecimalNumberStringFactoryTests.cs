﻿using System;
using System.Globalization;
using NUnit.Framework;
using Sipol.Numeric.Abstracts;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Abstracts
{
    [TestFixture]
    public class DecimalNumberStringFactoryTests
    {
        private DecimalNumberStringFactory<NumberString> sut;

        [SetUp]
        public void Setup()
        {
            sut = null;
        }

        #region DecimalSeparator

        [Test]
        public void DecimalSeparator__byDefault__IsNotNullAndIsNotEmpty()
        {
            // Arrange
            sut = CreateSUT();
            //Act
            var result = sut.DecimalSeparator;

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.Not.Empty
            );
        }

        #endregion
        
        #region Make

        [Test]
        public void Make__byDefault__ResultIsNotNullInstanceOfNumberStringAndOriginalWithDecimalSeparator()
        {
            // Arrange
            sut = CreateSUT();
            string decimalSep = ",";
            string numeric = $"12{decimalSep}5";
            
            //Act
            var result = sut.Make(numeric, decimalSep);

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>() &
                Has.Property(nameof(NumberString.Original)).Contains(decimalSep)
            );
        }

        [Test]
        public void Make__WithoutDecimalSeparator__ResultIsNotNullInstanceOfDecimalStringAndOriginalWithDecimalSeparator()
        {
            // Arrange
            sut = CreateSUT();
            sut.CultureInfo = CultureInfo.InvariantCulture;
            string decimalSep = sut.DecimalSeparator;
            
            string numeric = $"-12{decimalSep}5";
            
            //Act
            var result = sut.Make(numeric);

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>() &
                Has.Property(nameof(NumberString.Original)).Contains(decimalSep)
            );
        }

        [Test]
        public void Make__WithDouble__ResultIsNotNullInstanceOfDecimalStringAndOriginalWithDecimalSeparator()
        {
            // Arrange
            sut = CreateSUT();
            sut.CultureInfo = CultureInfo.InvariantCulture;
            string decimalSep = sut.DecimalSeparator;

            double tested = 15.2d;
            
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>() &
                Has.Property(nameof(NumberString.Original)).Contains(decimalSep)
            );

            Console.WriteLine(result.Value);
        }

        #endregion
        

        #region help funcs

        private DecimalNumberStringFactory<NumberString> CreateSUT()
        {
            sut = new Factory();
            return sut;
        }
        

        #endregion
        
        
        #region helper class

        private class Factory : DecimalNumberStringFactory<NumberString>
        {
            public override NumberString Make(string numeric, string decimalSeparator)
            {
                return NumberString.Factory.Make(numeric);
            }

            public override NumberString Make(INumberString numeric, string decimalSeparator) => Make(numeric?.Value, decimalSeparator);
        }
        
        #endregion
        
    }
}