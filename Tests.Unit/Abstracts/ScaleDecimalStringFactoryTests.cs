﻿using System;
using System.Collections.Generic;
using System.Globalization;
using NUnit.Framework;
using Sipol.Numeric.Abstracts;
using Sipol.Numeric.Impl;

namespace Tests.Unit.Abstracts
{
    [TestFixture]
    public class ScaleDecimalStringFactoryTests
    {
        private FactoryHelper sut;

        [SetUp]
        public void Setup()
        {
            sut = null;
        }

        #region Make_WithDouble

        [Test]
        public void Make_WithDouble__byDefault__IsNotNullInstanceOfNumberString()
        {
            // Arrange
            sut = CreateSUT();
            double tested = 0d;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>()
            );
            
            sut.ParamsToConsole();
        }

        [Test]
        public void Make_WithDouble__byDefault__CountMakeInvokeGreaterThanZero()
        {
            // Arrange
            sut = CreateSUT();
            double tested = 0d;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(sut.CountInvokedParams,
                    Is.GreaterThan(0)
                );

            sut.ParamsToConsole();
            
        }

        [Test]
        public void Make_WithDouble__WithFractionPart__MakeParamScaleIsEqualToScaleOfInputDouble()
        {
            // Arrange
            sut = CreateSUT();
            double tested = 0.01d;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(sut.MakeInvokedParams[0].Scale,
                Is.EqualTo(2)
            );

            sut.ParamsToConsole();
        }

        [Test]
        public void Make_WithDouble__WithFractionPartZero__MakeParamScaleIsEqualToZero()
        {
            // Arrange
            sut = CreateSUT();
            double tested = 0.000d;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(sut.MakeInvokedParams[0].Scale,
                Is.EqualTo(0)
            );

            sut.ParamsToConsole();
        }

        [Test]
        public void Make_WithDoubleAndScale__WithFractionPartZero__MakeParamScaleIsEqualToZero()
        {
            // Arrange
            sut = CreateSUT();
            double tested = 0.000d;
            int scale = 3;
            //Act
            var result = sut.Make(tested, scale);

            //Assert
            Assert.That(sut.MakeInvokedParams[0].Scale,
                Is.EqualTo(scale)
            );

            sut.ParamsToConsole();
        }
        #endregion

        #region Make_WithDecimal

        [Test]
        public void Make_WithDecimal__byDefault__ResultIsNotNullAndInstanceOfNumberString()
        {
            // Arrange
            
            sut = CreateSUT();
            decimal tested = 0m;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(result,
                Is.Not.Null &
                Is.InstanceOf<NumberString>()
            );
        }

        [Test]
        public void Make_WithDecimal__WithZeroDecimal__CountMakeInvokeGreaterThanZero()
        {
            // Arrange
            
            sut = CreateSUT();
            decimal tested = 0m;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(sut.CountInvokedParams,
                        Is.GreaterThan(0)
            );
        }

        [Test]
        public void Make_WithDecimal__WithFractionPart__IsEqualToFractionPartLength()
        {
            // Arrange
            
            sut = CreateSUT();
            decimal tested = 0.01m;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(sut.MakeInvokedParams[0].Scale,
                Is.EqualTo(2)
            );
        }

        [Test]
        public void Make_WithDecimalAndScale__WithFractionPart__IsEqualToInputScale()
        {
            // Arrange
            
            sut = CreateSUT();
            decimal tested = 0.0001m;
            //Act
            var result = sut.Make(tested, 4);

            //Assert
            Assert.That(sut.MakeInvokedParams[0].Scale,
                Is.EqualTo(4)
            );
        }
        
        [Test]
        public void Make_WithDecimal__ZeroWithFractionPart__IsEqualToFractionPartLength()
        {
            // Arrange
            
            sut = CreateSUT();
            decimal tested = 0.000m;
            //Act
            var result = sut.Make(tested);

            //Assert
            Assert.That(sut.MakeInvokedParams[0].Scale,
                Is.EqualTo(3)
            );
        }
        
        #endregion    
        
        
        #region help funcs

        private FactoryHelper CreateSUT()
        {
            sut = new FactoryHelper();
            sut.CultureInfo = CultureInfo.InvariantCulture;
            
            return sut;
        }

        #endregion
        

        #region helper class


        private class FactoryHelper : ScaleDecimalStringFactory<NumberString>
        {
            public int MakeInvokeCounter { get; set; } = 0;

            
            public List<MakeParams> MakeInvokedParams { get; private set; } = new List<MakeParams>();

            public int CountInvokedParams => MakeInvokedParams.Count;
            
            public override NumberString Make(string numeric, int scale, string decimalSeparator)
            {
                MakeInvokedParams.Add(
                    new MakeParams()
                    {
                        InvokeId  = ++MakeInvokeCounter,
                        Numeric = numeric,
                        Scale = scale,
                        DecimalSeparator = decimalSeparator
                    }
                    );

                return NumberString.Factory.Make(numeric);
            }

            public override NumberString Make(INumberString numeric, int scale, string decimalSeparator)
            {
                return Make(numeric?.Value, scale, decimalSeparator);
            }


            public void ParamsToConsole()
            {
                Console.WriteLine("------------------------------------------");
                Console.WriteLine("Make.params.count: {0}", CountInvokedParams);
                foreach (MakeParams p in MakeInvokedParams)
                    Console.WriteLine(p.ToString());
            }

            public class MakeParams
            {
                public int InvokeId { get; set; }
                public string Numeric { get; set; }
                public int Scale { get; set; }
                public string DecimalSeparator { get; set; }

                protected bool Equals(MakeParams other)
                {
                    return Numeric == other.Numeric && Scale == other.Scale && DecimalSeparator == other.DecimalSeparator;
                }

                public override bool Equals(object obj)
                {
                    if (ReferenceEquals(null, obj)) return false;
                    if (ReferenceEquals(this, obj)) return true;
                    if (obj.GetType() != this.GetType()) return false;
                    return Equals((MakeParams) obj);
                }

                public override int GetHashCode()
                {
                    unchecked
                    {
                        var hashCode = (Numeric != null ? Numeric.GetHashCode() : 0);
                        hashCode = (hashCode * 397) ^ Scale;
                        hashCode = (hashCode * 397) ^ (DecimalSeparator != null ? DecimalSeparator.GetHashCode() : 0);
                        return hashCode;
                    }
                }

                public override string ToString()
                {
                    return $"Make({nameof(InvokeId)}: [{InvokeId}], {nameof(Numeric)}: [{Numeric}], {nameof(Scale)}: [{Scale}], {nameof(DecimalSeparator)}: [{DecimalSeparator}])";
                }
            }
        }
        
        
        

        #endregion
    }
}