﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Sipol.Numeric.Impl;

namespace Sipol.Numeric.Abstracts
{
    public abstract class ScaleDecimalStringFactory<T>: DecimalNumberStringFactory<T> where T : NumberString
    {
        public const int ZeroScale = 0;

        public override T Make(string numeric, string decimalSeparator)
        {
            return Make(numeric, ZeroScale, decimalSeparator);
        }

        public abstract T Make(string numeric, int scale, string decimalSeparator);

        public virtual T Make(INumberString numeric, int scale) => Make(numeric, scale, DecimalSeparator);

        public override T Make(INumberString numeric, string decimalSeparator) => Make(numeric, 0, decimalSeparator);

        public abstract T Make(INumberString numeric, int scale, string decimalSeparator);
        
        
        
        public override T Make(double number)
        {
            int scale = 0;
            string inputDecimalNumber = number.ToString("G", CultureInfo.InvariantCulture);
            var regex = new Regex("(?<=[\\.])[0-9]+");
            if (regex.IsMatch(inputDecimalNumber))
                scale = regex.Match(inputDecimalNumber).Value.Length;

            return Make(number, scale);
        }


        public T Make(double number, int scale) => Make(Math.Round(number, (scale>0 ? scale:0)).ToString(DoubleFormat, CultureInfo), scale, DecimalSeparator);

        public T Make(float number, int scale) => Make((double) number, scale);
        
        public override T Make(decimal number)
        {
            int scale = 0;
            string inputDecimalNumber = number.ToString("G", CultureInfo.InvariantCulture);
            var regex = new Regex("(?<=[\\.])[0-9]+");
            if (regex.IsMatch(inputDecimalNumber))
                scale = regex.Match(inputDecimalNumber).Value.Length;

            return Make(number, scale);
        }

        public T Make(decimal number, int scale) => Make(Decimal.Round(number, (scale>0 ? scale:0)).ToString(DecimalFormat, CultureInfo), scale, DecimalSeparator);
    }
}