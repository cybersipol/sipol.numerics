﻿using Sipol.Numeric.Impl;

namespace Sipol.Numeric.Abstracts
{
    public interface INumberStringRounder
    {
        INumberString Round();
    }
}