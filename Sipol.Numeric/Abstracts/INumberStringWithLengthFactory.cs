﻿using Sipol.Numeric.Impl;

namespace Sipol.Numeric.Abstracts
{
    public interface INumberStringWithLengthFactory<T>: INumberStringFactory<T> where T : NumberString
    {
        T Make(string numeric, int numberLength);
    }
}