﻿using System;
using System.Globalization;
using System.Numerics;
using Sipol.Numeric.Impl;

namespace Sipol.Numeric.Abstracts
{
    public abstract class NumberStringFactory<T> : INumberStringFactory<T> where T : NumberString
    {
        
        protected virtual string Format => "0";
        protected virtual IFormatProvider FormatProvider => CultureInfo.InvariantCulture;

        #region abstracts
        
        public abstract T Make(string numeric);

        #endregion
        
        
        #region for decorators        
        public virtual T Make(INumberString number) => Make(number?.Value);
        
        #endregion
        
        #region other types
        
        public virtual T Make(BigInteger number) => Make(number.ToString(Format, FormatProvider));
        
        #endregion

        #region primitives
        
        public virtual T Make(ulong number) => Make(number.ToString(Format, FormatProvider));
        public virtual T Make(uint number) => Make((ulong) number);
        public virtual T Make(ushort number) => Make((ulong) number);
        public virtual T Make(byte number) => Make((ulong) number);


        public virtual T Make(long number) => Make(number.ToString(Format, FormatProvider));
        public virtual T Make(int number) => Make((long) number);
        public virtual T Make(short number) => Make((long) number);

        public virtual T Make(sbyte number) => Make((long) number);

        public virtual T Make(decimal number) => Make(Decimal.Round(number, 0).ToString(Format, FormatProvider));

        public virtual T Make(double number) => Make(Math.Round(number, 0).ToString(Format, FormatProvider));

        public virtual T Make(float number) => Make((double) number);

        #endregion
        
    }
}