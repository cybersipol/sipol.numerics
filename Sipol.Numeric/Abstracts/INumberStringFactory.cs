﻿using Sipol.Numeric.Impl;

namespace Sipol.Numeric.Abstracts
{
    public interface INumberStringFactory<out T> where T:NumberString
    {
        T Make(string numeric);
    }
}