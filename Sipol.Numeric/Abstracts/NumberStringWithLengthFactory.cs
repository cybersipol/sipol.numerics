﻿using System;
using System.Numerics;
using Sipol.Numeric.Impl;

namespace Sipol.Numeric.Abstracts
{
    public abstract class NumberStringWithLengthFactory<T> : NumberStringFactory<T>, INumberStringWithLengthFactory<T> where T : NumberString
    {
        #region abstracts

        public abstract T Make(string numeric, int numberLength);
        public abstract T Make(INumberString numeric, int numberLength);

        #endregion

 
        #region other types

        public virtual T Make(BigInteger number, int length) => Make(number.ToString(Format, FormatProvider), length);

        #endregion
        

        #region primitives

        public virtual T Make(ulong number, int length) => Make(number.ToString(Format, FormatProvider), length);
        public virtual T Make(uint number, int length) => Make((ulong) number, length);
        public virtual T Make(ushort number, int length) => Make((ulong) number, length);
        public virtual T Make(byte number, int length) => Make((ulong) number, length);

        public virtual T Make(long number, int length) => Make(number.ToString(Format, FormatProvider), length);
        public virtual T Make(int number, int length) => Make((long) number, length);
        public virtual T Make(short number, int length) => Make((long) number, length);

        public virtual T Make(sbyte number, int length) => Make((long) number, length);

        public virtual T Make(decimal number, int length) => Make(decimal.Round(number, 0).ToString(Format, FormatProvider), length);
        
        public virtual T Make(double number, int length) => Make(Math.Round(number, 0).ToString(Format, FormatProvider), length);
        
        public virtual T Make(float number, int length) => Make((double) number, length);

        

        #endregion
        
        
        #region overrides

        public override T Make(INumberString number) => Make(number, 0);

        public override T Make(string numeric) => Make(numeric, 0);

        public override T Make(BigInteger number) => Make(number, 0);
        
        public override T Make(ulong number) => Make(number, 0);

        public override T Make(uint number) => Make(number, 0);

        public override T Make(ushort number) => Make(number, 0);

        public override T Make(byte number) => Make(number, 0);

        public override T Make(long number) => Make(number, 0);

        public override T Make(int number) => Make(number, 0);

        public override T Make(short number) => Make(number, 0);

        public override T Make(sbyte number) => Make(number, 0);

        public override T Make(decimal number) => Make(number, 0);

        public override T Make(double number) => Make(number, 0);

        public override T Make(float number) => Make(number, 0);

        #endregion

    }
}