﻿using System;
using System.Globalization;
using System.Numerics;
using System.Threading;
using Sipol.Numeric.Impl;

namespace Sipol.Numeric.Abstracts
{
    public abstract class DecimalNumberStringFactory<T>: NumberStringFactory<T> where T : NumberString
    {
        protected static readonly string DoubleFormat = "G";
        public static readonly string DecimalFormat = "G";

        private readonly Lazy<CultureInfo> _lazyCurrentFormatProvider = new Lazy<CultureInfo>(() => Thread.CurrentThread.CurrentCulture, true);

        private CultureInfo _sCultureInfo = null;

        
        public string DecimalSeparator => CultureInfo.NumberFormat.NumberDecimalSeparator;
        
        public CultureInfo CultureInfo
        {
            get => _sCultureInfo ?? _lazyCurrentFormatProvider.Value;
            set => _sCultureInfo = value;
        }
        
        public abstract T Make(string numeric, string decimalSeparator);

        public override T Make(string numeric) => Make(numeric, DecimalSeparator);

        public override T Make(double number) => Make(number.ToString(DoubleFormat, CultureInfo), DecimalSeparator);

        public override T Make(decimal number) => Make(number.ToString(DecimalFormat, CultureInfo), DecimalSeparator);

        public abstract T Make(INumberString numeric, string decimalSeparator);

        public override T Make(INumberString number) => Make(number, DecimalSeparator);

        #region help funcs



        #endregion


    }
}