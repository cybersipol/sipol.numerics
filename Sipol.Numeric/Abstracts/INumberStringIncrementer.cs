﻿namespace Sipol.Numeric.Abstracts
{
    public interface INumberStringIncrementer
    {
        INumberString OriginalNumberString { get; }
        
        bool IsNextDigit { get; }
        
        INumberString Increment();
    }
}