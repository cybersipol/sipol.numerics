﻿namespace Sipol.Numeric.Abstracts
{
    public interface INumberString
    {
        bool IsZero { get; }
        
        string Original { get; }
        string Value { get; }
        
        int CountDigits { get; }
    }
}