﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class SignNumberString : NumberString
    {
        private static readonly Lazy<NumberStringFactory<SignNumberString>> LazyFactory = new Lazy<NumberStringFactory<SignNumberString>>(() => new _Factory(), true);

        private static readonly Lazy<SignNumberString> LazyZero = new Lazy<SignNumberString>(() => new SignNumberString(NumberString.Zero.Value), true);
        private static readonly Lazy<SignNumberString> LazyOne = new Lazy<SignNumberString>(() => new SignNumberString(NumberString.One.Value), true);
        private static readonly Lazy<SignNumberString> LazyMinusOne = new Lazy<SignNumberString>(() => new SignNumberString(MinusText + NumberString.One.Value), true);

        public const string MinusText = "-";
        public const string PlusText = "+";


        public new static NumberStringFactory<SignNumberString> Factory => LazyFactory.Value;

        public new static SignNumberString Zero => LazyZero.Value;

        public new static SignNumberString One => LazyOne.Value;

        public static SignNumberString MinusOne => LazyMinusOne.Value;


        private NumberString _absoluteNumberString = null;


        protected SignNumberString(string numericString) : base(numericString)
        {
            IsMinus = IsMinusString(numericString);
        }

        public override string Value
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(GetSign());

                return sb.Append(AbsoluteValue).ToString();
            }
        }

        public virtual string AbsoluteValue => base.Value;

        public NumberString Absolute
        {
            get
            {
                if (_absoluteNumberString is null)
                    _absoluteNumberString = NumberString.Factory.Make(AbsoluteValue);

                return _absoluteNumberString;
            }
        }

        public virtual bool IsMinus { get; }

        public string Sign => GetSign();


        private bool Equals(SignNumberString other)
        {
            return string.Equals(Value, other.Value);
        }


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) 
                return false;
            if (ReferenceEquals(this, obj)) 
                return true;

            return obj switch
            {
                SignNumberString sns => Equals(sns),
                NumberString ns => Equals(Factory.Make(ns.Value)),
                _ => false
            };
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        #region help funcs

        private string GetSign() => IsMinus && !IsZero ? MinusText : string.Empty;

        #endregion    
        
        
        
        #region static funcs

        public static bool IsMinusString(string value)
        {
            if (!IsNumericString(value))
                return false;


            return GetMinusLocationInString(value) > GetPlusLocationInString(value);
        }


        private static string GetSignString(string value) => IsMinusString(value) && !IsZeroString(value) ? MinusText : string.Empty;

        private static int GetMinusLocationInString(string value) => GetNumericWithSingsStringValue(value).IndexOf(MinusText, StringComparison.Ordinal);
        private static int GetPlusLocationInString(string value) => GetNumericWithSingsStringValue(value).IndexOf(PlusText, StringComparison.Ordinal);
        
        
        
        private static string GetNumericWithSingsStringValue(string value)
        {
            return GetClearedNotDigitsAtLast(
                        GetClearedNotDigitsBetweenDigits(
                                RegexMultiReplace(
                                    value ?? String.Empty, 
                                    @"([^0-9\\" + PlusText + @"\\" + MinusText + "]+)", 
                                    String.Empty
                                    )
                            )
                        );
        }

        private static string GetClearedNotDigitsBetweenDigits(string value) => RegexMultiReplace(value, "([0-9]+)([^0-9]+)([0-9]+)", "$1$3");

        private static string GetClearedNotDigitsAtLast(string value) => RegexMultiReplace(value , "([^0-9]+)$", "");


        private static string RegexMultiReplace(string input, string pattern, string replacement)
        {
            string result = input ?? String.Empty;
            while (Regex.IsMatch(result, pattern))
                result = Regex.Replace(result, pattern, replacement);

            return result;
        }

        #endregion;

        private class _Factory : NumberStringFactory<SignNumberString>
        {
            public override SignNumberString Make(string numeric)
            {
                return GetDefaultObjectOrNull(numeric) ?? new SignNumberString(numeric);
            }

            public override SignNumberString Make(INumberString number)
            {
                return Make(GetSignString(number?.Original) + number?.Value);
            }

            
            
            private static SignNumberString GetDefaultObjectOrNull(string numeric)
            {
                if (IsZeroString(numeric))
                    return Zero;

                if (IsOneString(numeric))
                    return IsMinusString(numeric) ? MinusOne : One;

                return null;
            }
        }
    }
}