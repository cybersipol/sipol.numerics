﻿using System;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class NumberStringEndingZeros: NumberString
    {
        private static readonly Lazy<NumberStringWithLengthFactory<NumberStringEndingZeros>> LazyFactory = new Lazy<NumberStringWithLengthFactory<NumberStringEndingZeros>>(() => new _Factory(), true);
        
        private const char LeadingChar = '0';

        private readonly int _length;

        public new static NumberStringWithLengthFactory<NumberStringEndingZeros> Factory => LazyFactory.Value;
        
        
        protected NumberStringEndingZeros(string numeric, int length=0) : base(numeric)
        {
            _length = length;
        }

        public override string Value => base.Value.Length >= _length ? base.Value : base.Value.PadRight(_length, LeadingChar);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) 
                return false;
            if (ReferenceEquals(this, obj)) 
                return true;
            
            if (obj is NumberStringEndingZeros numberStringEndingZeros) 
                return Equals(this, numberStringEndingZeros);

            if (obj is NumberString numberString) 
                return Equals(this, Factory.Make(numberString.Value, 0));

            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        #region static funcs


        public static bool Equals(NumberStringEndingZeros n1, NumberStringEndingZeros n2)
        {
            int maxLength = Math.Max(GetValueOrEmpty(n1).Length, GetValueOrEmpty(n2).Length);

            return String.Equals(GetValueOrEmpty(n1).PadRight(maxLength, LeadingChar), GetValueOrEmpty(n2).PadRight(maxLength, LeadingChar));
        }

        #endregion

        #region help funcs

        

        #endregion

        
        
        #region factory class

        private class _Factory : NumberStringWithLengthFactory<NumberStringEndingZeros>
        {
            public override NumberStringEndingZeros Make(string numeric, int numberLength) => new NumberStringEndingZeros(numeric, numberLength);
            public override NumberStringEndingZeros Make(INumberString numeric, int numberLength) => new NumberStringEndingZeros(numeric?.Value, numberLength);
        }
        

        #endregion
        
        
    }
}