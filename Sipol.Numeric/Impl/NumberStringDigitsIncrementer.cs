﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class NumberStringDigitsIncrementer : INumberStringIncrementer
    {
        public bool IsNextDigit { get; private set; } = false;

        private List<byte> _digits;
        
        private readonly INumberString _numberString;

        public NumberStringDigitsIncrementer(INumberString numberString)
        {
            _numberString = numberString ?? throw new ArgumentNullException(nameof(numberString));
        }

        public INumberString OriginalNumberString => _numberString;

        
        public INumberString Increment()
        {
            CreateOrderedBytesFromNumberString();
            IncrementDigit(0);

            return NumberString.Factory.Make(DigitsToString());
        }


        #region help funcs

        private void CreateOrderedBytesFromNumberString()
        {
            _digits = new List<byte>();
            
            for (int i = _numberString.Value.Length - 1; i >=0; i--)
            {
                char numericChar = _numberString.Value[i];
                if (!Char.IsDigit(numericChar))
                    continue;

                byte digit = byte.Parse($"{numericChar}");
                _digits.Add(digit);
            }
            
        }

        private void IncrementDigit(int digitIndex)
        {
            if (digitIndex >= _digits.Count)
            {
                IsNextDigit = true;
                return;
            }

            byte digit = _digits[digitIndex];

            if (digit >= 9)
            {
                _digits[digitIndex] = 0;
                IncrementDigit(digitIndex+1);
                return;
            }

            _digits[digitIndex] = (byte)(digit + 1);
        }

        
        private string DigitsToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = _digits.Count - 1; i >= 0; i--)
            {
                int digit = _digits[i];
                sb.Append(digit.ToString("0", CultureInfo.InvariantCulture));
            }

            return sb.ToString();
        }


        #endregion
        
    }
}