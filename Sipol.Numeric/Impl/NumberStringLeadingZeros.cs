﻿using System;
using System.Globalization;
using System.Numerics;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class NumberStringLeadingZeros: NumberString
    {
        private static readonly Lazy<NumberStringWithLengthFactory<NumberStringLeadingZeros>> LazyFactory = new Lazy<NumberStringWithLengthFactory<NumberStringLeadingZeros>>(() => new _Factory(), true);
        
        private static readonly char LeadingChar = '0';

        public new static NumberStringWithLengthFactory<NumberStringLeadingZeros> Factory => LazyFactory.Value;
        
        private readonly int _length;

        protected NumberStringLeadingZeros(string numeric, int length = 0) : base(numeric)
        {
            _length = length;
        }

        public override string Value => base.Value.Length >= _length ? base.Value : base.Value.PadLeft(_length, LeadingChar);


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;

            return obj switch
            {
                NumberStringLeadingZeros leadingZeros => Equals(leadingZeros),
                NumberString numberString => Equals(numberString),
                _ => EqualsOthersTypes(obj)
            };
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }


        #region help funcs

        private bool Equals(NumberString other)
        {
            return Equals(this, Factory.Make(other.Value,0));
        }

        private bool Equals(NumberStringLeadingZeros other)
        {
            return Equals(this, other);
        }


        private bool EqualsOthersTypes(object obj)
        {
            return obj switch
            {
                string str => Equals(Factory.Make(str, 0)), 
                BigInteger bi => Equals(Factory.Make(bi)),
                decimal dec => Equals(Factory.Make(dec)),
                ulong ul => Equals(Factory.Make(ul)),
                uint ui => Equals(Factory.Make(ui)),
                ushort us => Equals(Factory.Make(us)),
                long l => Equals(Factory.Make(l)),
                int i => Equals(Factory.Make(i)),
                short s => Equals(Factory.Make(s)),
                byte b => Equals(Factory.Make(b)),
                sbyte sb => Equals(Factory.Make(sb)),
                double db => Equals(Factory.Make(db)),
                float fl => Equals(Factory.Make(fl)),
                _ => false
            };
        }

        #endregion

        #region static funcs

        public static bool Equals(NumberStringLeadingZeros n1, NumberStringLeadingZeros n2)
        {
            int maxLength = Math.Max(GetValueOrEmpty(n1).Length , GetValueOrEmpty(n2).Length);

            return String.Equals(GetValueOrEmpty(n1).PadLeft(maxLength, LeadingChar), GetValueOrEmpty(n2).PadLeft(maxLength, LeadingChar));
        }
        
        #endregion


        #region factory class


        private class _Factory : NumberStringWithLengthFactory<NumberStringLeadingZeros>
        {
            public override NumberStringLeadingZeros Make(string numeric, int numberLength) => new NumberStringLeadingZeros(numeric, numberLength);
            public override NumberStringLeadingZeros Make(INumberString numeric, int numberLength) => new NumberStringLeadingZeros(numeric?.Value, numberLength);
        }

        #endregion
    }
}