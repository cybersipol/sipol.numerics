﻿using System;
using System.Globalization;
using System.Text;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class DecimalString: SignNumberString
    {
        private static readonly Lazy<DecimalNumberStringFactory<DecimalString>> LazyFactory = new Lazy<DecimalNumberStringFactory<DecimalString>>(() => new _Factory(), true); 
        
        private static readonly Lazy<DecimalString> LazyZero = new Lazy<DecimalString>(() => Factory.Make(NumberString.Zero.Value), true);

        public new static DecimalNumberStringFactory<DecimalString> Factory => LazyFactory.Value;
        
        public new static DecimalString Zero => LazyZero.Value;


        private readonly SignNumberString _integerString;
        private readonly NumberString _fractionString;
        
        
        private int? _decimalSeparatorLocation = null;
        private bool? _isNumeric = null;
        private string _decimalSeparator = null;

        public CultureInfo CultureInfo
        {
            get => Factory.CultureInfo;
            set => Factory.CultureInfo = value;
        }


        protected DecimalString(string numericString, string decimalSeparator = null): base(numericString)
        {
            _decimalSeparator = decimalSeparator;
            _integerString = CreateIntegerNumberString();
            _fractionString = CreateFractionNumberString();
            IsMinus = !IsZero && IsMinusString(numericString);
        }


        private int DecimalSeparatorLocation
        {
            get
            {
                if (_decimalSeparatorLocation is null)
                    _decimalSeparatorLocation = GetDecimalSeparatorLocation();

                return _decimalSeparatorLocation.Value;
            }
        }

        
        public string DecimalSeparator => GetDecimalSeparator();



        public virtual SignNumberString IntegerPart => _integerString;

        public virtual NumberString AbsoluteIntegerPart => IntegerPart.Absolute;


        public virtual NumberString FractionPart => _fractionString;
        

        public bool HasDecimalSeparator => DecimalSeparatorLocation >= 0;

        public new bool IsZero => _integerString.IsZero && _fractionString.IsZero;

        private bool IsNumeric => GetIsNumeric();

        public override string Value => GetValue();


        public override bool IsMinus { get; }


        public override string ToString()
        {
            if (_fractionString.IsZero)
                return IntegerPart.Value;

            return Value;
        }

        #region help funcs

        private string GetDecimalSeparator()
        {
            if (_decimalSeparator is null)
                _decimalSeparator = Factory.DecimalSeparator;

            return _decimalSeparator;
        }
        
        
        private int GetDecimalSeparatorLocation()
        {
            if (!IsNumeric)
                return -1;

            if (_decimalSeparatorLocation is null)
                _decimalSeparatorLocation = Original.LastIndexOf(DecimalSeparator, StringComparison.Ordinal);

            return _decimalSeparatorLocation.Value;
        }


        private bool GetIsNumeric()
        {
            if (_isNumeric is null)
                _isNumeric = IsNumericString(Original);

            return _isNumeric.Value;
        }

        
        private SignNumberString CreateIntegerNumberString()
        {
            if (!IsNumeric)
                return SignNumberString.Zero;

            return SignNumberString.Factory.Make(HasDecimalSeparator ? GetIntegerPartString() : Original);
        }




        private NumberString CreateFractionNumberString()
        {
            return HasDecimalSeparator ? NumberString.Factory.Make(GetFractionPartString()) : NumberString.Zero;
        }

        private string GetIntegerPartString()
        {
            return Original.Substring(0, DecimalSeparatorLocation);
        }

        private string GetFractionPartString()
        {
            return Original?.Substring(DecimalSeparatorLocation + 1, Original.Length - DecimalSeparatorLocation - 1) ?? NumberString.Zero.Value;
        }

        private string GetValue()
        {
            if (!IsNumeric)
                return NumberString.Zero.Value;

            return HasDecimalSeparator ? GlueIntegerAndFractionPartIfExistsDecimalSeparator(): IntegerPart.Value;
        }
        
        

        public string GlueIntegerAndFractionPartIfExistsDecimalSeparator()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(IntegerPart.Value);

            if (HasDecimalSeparator)
                sb
                    .Append(DecimalSeparator)
                    .Append(FractionPart.Value);

            return sb.ToString();
        }
        
        public string GlueIntegerAndFractionPart()
        {
            StringBuilder sb = new StringBuilder();
            sb
                .Append(IntegerPart.Value)
                .Append(DecimalSeparator)
                .Append(FractionPart.Value)
                ;

            return sb.ToString();
        }

        #endregion

        #region factory class

        private class _Factory : DecimalNumberStringFactory<DecimalString>
        {
            public override DecimalString Make(string numeric, string decimalSeparator) => new DecimalString(numeric, decimalSeparator);
            public override DecimalString Make(INumberString numeric, string decimalSeparator) => Make(numeric?.Value, decimalSeparator);
        }
        
        
        #endregion

    }
}