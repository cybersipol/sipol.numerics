﻿using System;
using System.Numerics;
using System.Text.RegularExpressions;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class NumberString : INumberString
    {
        private static readonly Lazy<NumberStringFactory<NumberString>> LazyFactory = new Lazy<NumberStringFactory<NumberString>>(() => new _Factory(), true); 
        
        private static readonly Lazy<NumberString> LazyZero = new Lazy<NumberString>(() => new NumberString(ZeroNumberString), true);
        private static readonly Lazy<NumberString> LazyOne = new Lazy<NumberString>(() => new NumberString(OneNumberString), true);

        private static readonly string ZeroNumberString = "0";
        private static readonly string OneNumberString = "1";


        public static NumberStringFactory<NumberString> Factory => LazyFactory.Value;

        public static NumberString Zero => LazyZero.Value;
        public static NumberString One => LazyOne.Value; 
        

        private string _value;

        protected NumberString(string numeric)
        {
            _value = null;
            Original = numeric;
        }

        public string Original { get; }

        public virtual string Value => GetValue();
        public int CountDigits => Value?.Length ?? 0;

        public bool IsZero => GetIsZero();

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;

            if (obj is NumberString nb)
                return Equals(nb);

            return EqualsOtherTypes(obj);
        }

        
        
        
        public override int GetHashCode() => Value.GetHashCode();

        public override string ToString() => Value;


        #region help funcs

        private string GetValue()
        {
            if (_value is null)
                _value = GetNumericString(Original);

            return String.IsNullOrWhiteSpace(_value) ? ZeroNumberString : _value;
        }

        private bool GetIsZero() => IsZeroString(GetValue());

        private  bool Equals(NumberString other)
        {
            return Equals(this, other);
        }

        private bool EqualsOtherTypes(object obj)
        {
            return obj switch
            {
                string str => Equals(Factory.Make(str)),
                BigInteger bi => Equals(Factory.Make(bi)),
                decimal dec => Equals(Factory.Make(dec)),
                ulong ul => Equals(Factory.Make(ul)),
                uint ui => Equals(Factory.Make(ui)),
                ushort us => Equals(Factory.Make(us)),
                long l => Equals(Factory.Make(l)),
                int i => Equals(Factory.Make(i)),
                short s => Equals(Factory.Make(s)),
                byte b => Equals(Factory.Make(b)),
                sbyte sb => Equals(Factory.Make(sb)),
                double db => Equals(Factory.Make(db)),
                float fl => Equals(Factory.Make(fl)),
                _ => false
            };
        }

        
        #endregion
        
        #region static funcs
        
        public static string GetNumericString(string value)
        {
            return Regex.Replace(value ?? string.Empty, "([^0-9]+)", string.Empty);
        }


        public static bool IsNumericString(string value)
        {
            return !string.IsNullOrWhiteSpace(value) && Regex.IsMatch(value, "([0-9]+)");
        }

        public static bool IsZeroString(string value)
        {
            if (!IsNumericString(value))
                return true;
            
            return Regex.IsMatch(GetNumericString(value), "^(" + ZeroNumberString + "+)$");
        }

        public static bool IsOneString(string value)
        {
            if (!IsNumericString(value))
                return false;
            
            return Regex.IsMatch(GetNumericString(value), "^" + OneNumberString + "$");
        }
        
        public static bool Equals(NumberString value1, NumberString value2)
        {
            return string.Equals(GetValueOrEmpty(value1), GetValueOrEmpty(value2));
        }


        public static string GetValueOrEmpty(NumberString n) => n?.Value ?? String.Empty;


        #endregion


        #region Factory class

        
        
        private class _Factory : NumberStringFactory<NumberString>
        {
            public override NumberString Make(string numeric)
            {
                if (IsZeroString(numeric))
                    return Zero;
            
                return IsOneString(numeric) ? One : new NumberString(numeric);

            }
        }

        #endregion
    }
}