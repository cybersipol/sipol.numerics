﻿using System;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class NumberStringDigitsIncrementNextDigitDecorator: INumberStringIncrementer
    {
        private INumberStringIncrementer _incrementer;

        public NumberStringDigitsIncrementNextDigitDecorator(INumberStringIncrementer incrementer)
        {
            _incrementer = incrementer ?? throw new ArgumentNullException(nameof(incrementer));
        }

        public INumberString OriginalNumberString => _incrementer.OriginalNumberString;
        public bool IsNextDigit => _incrementer.IsNextDigit;
        public INumberString Increment()
        {
            INumberString incNum = _incrementer.Increment();
            if (IsNextDigit)
                return NumberString.Factory.Make(NumberString.One.Value + NumberStringLeadingZeros.Factory.Make(incNum.Value, OriginalNumberString.CountDigits));

            return incNum;
        }
    }
}