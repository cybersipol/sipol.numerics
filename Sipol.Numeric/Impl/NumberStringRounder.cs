﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class NumberStringRounder: INumberStringRounder
    {
        private const string DigitZero = "0";
        
        private readonly string _numeric = null;

        private readonly int _digitNumber;

        private string _numericAccepted;
        private string _numericRejected;

        private INumberStringIncrementer _incrementer;

        public bool IsNextDigit => _incrementer?.IsNextDigit ?? false;

        public NumberStringRounder(INumberString number, int digitNumber)
        {
            var numberOrZero = number ?? NumberString.Zero;
            
            _numeric = GetNumericString(numberOrZero.Value);
            _digitNumber = GetProperDigitNumber(digitNumber);
        }


        public INumberString Round()
        {
            CreateNumericStringAccepted();
            CreateNumericStringRejected();

            return GenerateNumberString(GetRoundedNumericStringAccepted());
        }

        
        
        
        #region help funcs

        private string GetNumericString(string numeric) => NumberString.GetNumericString(numeric).PadLeft(1, '0');

        private int GetProperDigitNumber(int digitNumber)
        {
            if (digitNumber > _numeric.Length)
                return _numeric.Length;

            if (digitNumber < 0)
                return GetProperDigitNumber((_numeric.Length + digitNumber + 1)<0 ? 0:(_numeric.Length + digitNumber + 1));


            return digitNumber;
        }


        
        
        private void CreateNumericStringAccepted() => _numericAccepted = _numeric.Substring(0, _digitNumber);

        private void CreateNumericStringRejected() => _numericRejected = (_digitNumber >= _numeric.Length) ? DigitZero : _numeric.Substring(_digitNumber);



        
        private string GetRoundedNumericStringAccepted()
        {
            byte lastDigit = GetFirstDigitRejected();
            
            if (lastDigit >= 5)
                return IncrementDigit();

            return _numericAccepted;
        }

        private string IncrementDigit()
        {
            _incrementer = new NumberStringDigitsIncrementer(NumberString.Factory.Make(String.IsNullOrEmpty(_numericAccepted) ? "9":_numericAccepted));

            return _incrementer.Increment().Value;
        }

        private NumberString GenerateNumberString(string roundedString) => NumberStringEndingZeros.Factory.Make(roundedString, _numeric.Length);

        private byte GetFirstDigitRejected() => byte.Parse(_numericRejected?.Substring(0,1) ?? DigitZero);







        
        #endregion
    }
}