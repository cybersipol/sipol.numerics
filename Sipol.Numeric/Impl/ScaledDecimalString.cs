﻿using System;
using Sipol.Numeric.Abstracts;

namespace Sipol.Numeric.Impl
{
    public class ScaledDecimalString : DecimalString
    {
        private static readonly Lazy<ScaleDecimalStringFactory<ScaledDecimalString>> LazyFactory = new Lazy<ScaleDecimalStringFactory<ScaledDecimalString>>(() => new _Factory(), true);

        public new static ScaleDecimalStringFactory<ScaledDecimalString> Factory => LazyFactory.Value;
        
        protected ScaledDecimalString(string numericString, int scale = 0, string decimalSeparator = null) : base(numericString, decimalSeparator)
        {
            Scale = scale;
        }


        private SignNumberString _integerPart = null;
        private NumberString _fractionPart = null;
        
        public int Scale { get; }

        #region override properties

        public override SignNumberString IntegerPart => GetIntegerPart();

        public override NumberString FractionPart => GetFractionalPart();

        
        public override string Value => GetValue();

        #endregion


        #region help funcs

        private SignNumberString GetIntegerPart()
        {
            if (_integerPart is null)
                _integerPart = CreateIntegerPart();

            return _integerPart;
        }

        private SignNumberString CreateIntegerPart()
        {
            if (Scale >= 0)
                return GetIntegerPartForScaleEqualToZeroOrGreater();

            return CreateSignNumberStringFromIntegerPart();
        }

        private SignNumberString CreateSignNumberStringFromIntegerPart() => CreateSignNumberStringFromAbsoluteNumberString(GetRoundedAbsoluteIntegerPart());
        
        private SignNumberString CreateSignNumberStringFromAbsoluteNumberString(INumberString absolute)
        {
            return SignNumberString.Factory.Make(Sign + absolute.Value);
        }

        private INumberString GetRoundedAbsoluteIntegerPart()
        {
            return new NumberStringRounder(CreateNumberStringLeadingZerosWithScaleForBaseAbsoluteIntegerPart(), Scale - 1).Round();
        }


        private NumberStringLeadingZeros CreateNumberStringLeadingZerosWithScaleForBaseAbsoluteIntegerPart()
        {
            return NumberStringLeadingZeros.Factory.Make(base.IntegerPart.AbsoluteValue, Math.Abs(Scale - 1));
        }

        private SignNumberString GetIntegerPartForScaleEqualToZeroOrGreater()
        {
            if (IsNextDigitFractionPartFullRound())
                return SignNumberString.Factory.Make((IsMinus ? MinusText : String.Empty) + IncrementIntegerPartAbsolute().Value);

            return base.IntegerPart;
        }

        private bool IsNextDigitFractionPartFullRound()
        {
            var rounderFractionalPart = new NumberStringRounder(base.FractionPart ?? NumberString.Zero, 0);
            rounderFractionalPart.Round();

            return rounderFractionalPart.IsNextDigit;
        }

        private INumberString IncrementIntegerPartAbsolute()
        {
            return new NumberStringDigitsIncrementNextDigitDecorator(new NumberStringDigitsIncrementer(base.IntegerPart.Absolute)).Increment();
        }

        private NumberString GetFractionalPart()
        {
            if (_fractionPart is null)
                _fractionPart = CreateFractionPart();

            return _fractionPart;
        }    

        private NumberString CreateFractionPart()
        {
            if (Scale <= 0)
                return NumberString.Zero;

            return IsScaleOverBaseFractionValueLength()
                    ? CreateNumberStringEndingZerosFromBaseFractionPart()
                    : CreateNumberStringEndingZerosFromRoundedBaseFractionPart()
                ;
        }

        private bool IsScaleOverBaseFractionValueLength()
        {
            return Scale > (base.FractionPart ?? NumberString.Zero).Value.Length;
        }

        private NumberStringEndingZeros CreateNumberStringEndingZerosFromBaseFractionPart() => NumberStringEndingZeros.Factory.Make(base.FractionPart.Value, Scale);

        private NumberStringEndingZeros CreateNumberStringEndingZerosFromRoundedBaseFractionPart()
        {
            return NumberStringEndingZeros.Factory.Make(GetValueRoundedBaseFractionPartWithScale(), Scale);
        }
        
        private string GetValueRoundedBaseFractionPartWithScale() => CreateRoundedBaseFractionPart().Value.Substring(0, Scale);
        
        private INumberString CreateRoundedBaseFractionPart() => new NumberStringRounder(base.FractionPart ?? NumberString.Zero, Scale).Round();


        private string GetValue()
        {
            if (Scale<=0)
                return IntegerPart.Value;

            return GlueIntegerAndFractionPart();
        }


        #endregion
        
        
        #region factory class

        private class _Factory : ScaleDecimalStringFactory<ScaledDecimalString>
        {
            public override ScaledDecimalString Make(string numeric, int scale, string decimalSeparator) => new ScaledDecimalString(numeric, scale, decimalSeparator);
            
            public override ScaledDecimalString Make(INumberString numeric, int scale, string decimalSeparator) => Make(numeric?.Value, scale, decimalSeparator);
        }

        #endregion
    }
}